
### Running

##### 0. Install `pyenv`.
If you have pyenv on your computer, skip this step. Otherwise, follow next:

- Execute in terminal
    >`curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash`
- Add lines in ~/.bashrc:
    >`export PATH="~/.pyenv/bin:$PATH"`
    >`eval "$(pyenv init -)"`
    >`eval "$(pyenv virtualenv-init -)"`
- Execute in terminal:
    >`exec bash`
- Execute in terminal:
    >`pyenv update`

##### 1. Create `virtualenv`:
Create `virtualenv` using `pyenv`:
- Install appropriate version of `python`:
    >`pyenv install 3.6.4`
- Create `virtualenv` named `signals`:
    >`pyenv virtualenv 3.6.4 signals`

##### 2. Clone project:
- Execute being in your workspace directory:
    >`git clone git@bitbucket.org:lupa_andrew/signals.git`

##### 3. Run service:
Run beeing in project path.
- Launch `virtualenv` if needed:
    >`pyenv local signals`
- Run:
    >`python3 app.py --secrets=/path/to/config --host=host_to_serve --port=port`
