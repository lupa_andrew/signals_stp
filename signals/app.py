import asyncio
import logging
import logging.config

import aiohttp_jinja2
import jinja2
import yaml

from aiohttp import web
from aiohttp_middlewares import timeout_middleware

from signals.config import make_parser, get_config
from signals.db import configure_db, disconnect_db
from signals.errors import error
from signals.routes import setup_routes


config = yaml.load(open('config/logging.yaml'))
logging.config.dictConfig(config['logging'])

log = logging.getLogger('app')


def error_pages(overrides):
    @web.middleware
    async def middleware(request, handler):
        try:
            response = await handler(request)
            override = overrides.get(response.status)
            if override is None:
                return response
            else:
                return override(request, response)

        except asyncio.TimeoutError as ex:
            log.exception(
                'Timeout error',
                extra={
                    'request': request.method,
                    'url': str(request.rel_url),
                }
            )
            override = overrides.get(504)
            return override(request, ex)
        except web.HTTPException as ex:
            override = overrides.get(ex.status)
            if override is None:
                raise
            else:
                return override(request, ex)
    return middleware


def create_app(timeout, db_config):
    app = web.Application(
        logger=log,
        client_max_size=1024 * 1024 * 10,
    )

    app['db_config'] = db_config

    # Signal handlers
    app.on_startup.extend([
        configure_db,
    ])
    app.on_cleanup.extend([
        disconnect_db,
    ])

    # Middleware
    app.middlewares.extend([
        error_pages({
            404: lambda req, resp: error(404),
            500: lambda req, resp: error(500),
            504: lambda req, resp: error(504),
        }),
        timeout_middleware(timeout),
    ])

    # Add templating
    template_loader = jinja2.FileSystemLoader('./templates')
    aiohttp_jinja2.setup(app, loader=template_loader)

    # Add static routes
    app.router.add_static('/static', './public')

    setup_routes(app)

    log.info('Application configured!')

    return app


def run_app():

    ap = make_parser()
    args = ap.parse_args()
    config = get_config(args.secrets)
    db_config = config['db']
    host, port = args.host, args.port

    web.run_app(
        create_app(
            args.timeout,
            db_config,
        ),
        host=host,
        port=port,
    )
