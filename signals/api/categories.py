import logging

from aiohttp import web

from signals.utils.db import to_list, insert


log = logging.getLogger('app')


async def create_category(request):
    data = dict(request.json())
    db = request.app['db']
    category = await insert(db.categories, data)
    log.info(f'Category(id={category["_id"]}, name={category["name"]}) created!')
    return web.json_response(category)


async def find_categories(request):
    db = request.app['db']
    data = to_list(db.categories.find())
    log.info(data)
    return web.json_response(data)
