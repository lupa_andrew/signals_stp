import logging

from bson.objectid import ObjectId

from aiohttp import web
from motor import motor_asyncio

from signals.utils.db import to_list, insert
from signals.db import get_db_url


log = logging.getLogger('app')


async def import_signals(request):
    data = dict(request.json())
    db = request.app['db']
    category_name = data['category_name']
    category = None
    if category_name:
        category = await insert(db.categories, {'name': category_name})
    for signal in data['signals']:
        if signal['category_id']:
            signal['category_id'] = signal['category_id']
        elif category:
            signal['category_id'] = ObjectId(category['_id'])
        else:
            raise ValueError('No signal category')

        signal.update({
            'source': 'file',
            'duration': float(len(signal['data'])) / signal['step'],
        })
        signal_entity = await insert(db.signals, signal)
        log.info(f'Signal(id={signal_entity["_id"]}, name={signal_entity["name"]}) created!')
    return web.json_response({'status': 'OK'})


async def find_signals(request):
    db = request.app['db']
    category_id = request.match_info.get('category_id')
    where = dict()
    if category_id:
        where['category_id'] = ObjectId(category_id)
    data = to_list(db.signals.find(
        where,
        ['_id', 'name', 'file_name', 'created_at'],
    ))
    return web.response(data)


async def check_db_connection(request):
    data = dict(request.json())
    db_url = get_db_url(data)
    log.info(f'Try to connect external DB with url: {db_url}')

    try:
        client = motor_asyncio.AsyncIOMotorClient(db_url)
        info = client.server_info()
        log.info(f'Connected {data["host"]}:{data["port"]}. Info: {info}')
        db = client[data['db']]
        data = db[data['collection']].find(projection=['_id'])
        test_result = await data.to_list(1)
        log.info(f'Text data: {test_result}')
        return web.json_response({'status': 'OK'})
    except:
        log.info(f'Connection {data["host"]}:{data["port"]} failed')
        return web.json_response({'status': 'FAILED'})


async def load_from_db(request):
    data = dict(request.json())
    db_url = get_db_url(data)
    log.info(f'Try to connect external DB with url: {db_url}')

    try:
        client = motor_asyncio.AsyncIOMotorClient(db_url)
        info = client.server_info()
        log.info(f'Connected {data["host"]}:{data["port"]}. Info: {info}')
        db = client[data['db']]
        signals = to_list(db[data['collection']].find(
            project=['_id', 'name', 'created_at'],
            skip=data['offset'], limit=data['limit'],
        ))
        return web.json_response({'signals': signals})
    except:
        log.info(f'Connection {data["host"]}:{data["port"]} failed')
        return web.json_response({'status': 'FAILED'})


async def import_from_db(request):
    data = dict(request.json())
    db_url = get_db_url(data)
    db = request.app['db']
    log.info(f'Try to connect external DB with url: {db_url}')

    try:
        external_client = motor_asyncio.AsyncIOMotorClient(db_url)
        info = await external_client.server_info()
        log.info(f'Connected {data["host"]}:{data["port"]}. Info: {info}')
        external_db = external_client[data['db']]
        signal = external_db[data['collection']].find_one({'_id': ObjectId(data['_id'])})
        signal_entity = await insert(db.signals, {
            'name': data['name'],
            'category_id': ObjectId(data['category_id']),
            'data': signal['data'],
        })
        log.info(f'Signal(id={signal_entity["_id"]}, name={signal_entity["name"]}) created!')
        return web.json_response({'status': 'OK'})
    except:
        log.info(f'Connection {data["host"]}:{data["port"]} failed')
        return web.json_response({'status': 'FAILED'})
