import logging

from bson.objectid import ObjectId
from copy import deepcopy

from aiohttp import web
from motor import motor_asyncio

from signals.utils.db import to_list, insert, to_json
from signals.db import get_db_url


log = logging.getLogger('app')


async def find(request):
    db = request.app['db']
    signal_id = request.match_info['signals_id']
    data = await db.signals.find_one({'_id': ObjectId(signal_id)})
    data = to_json(data)
    return web.json_response(data)


async def delete(request):
    db = request.app['db']
    signal_id = request.match_info.get('signal_id')
    await db.signals.delete_one({'_id': ObjectId(signal_id)})
    return web.json_response({'status': 'OK'})


async def update(request):
    db = request.app['db']
    data = dict(request.json())
    signal_id = request.match_info.get('signal_id')
    log.info(signal_id)
    data['category_id'] = ObjectId(data['category_id'])
    log.info(data)
    await db.signals.update_one({'_id': ObjectId(signal_id)}, {'$set': data})
    log.info(data, signal_id)
    log.info(f'Signal(id={signal_id}, name={data["name"]}) updated!')
    return web.json_response({'status': 'OK'})


async def cut(request):
    db = request.app['db']
    data = dict(request.json())
    signal_id = request.match_info.get('signal_id')
    signal = await db.signals.find_one({'_id': ObjectId(signal_id)})
    cut_from = data['from']
    cut_to = data['to']
    first_part = signal['data'][:cut_from]
    second_part = signal['data'][cut_from:cut_to]
    third_part = signal['data'][cut_to:]
    signal_data = {
        'category_id': ObjectId(signal['category_id']),
        'step': signal['step'],
        'type': ObjectId(signal['type']),
    }
    signal_comment = signal.get('comment', '')
    log.info(signal_data)
    if data['partOneSave']:
        first_signal = deepcopy(signal_data)
        first_signal.update({
            'name': f'{signal["name"]} part I',
            'comment': f'{signal_comment}. Cut from {signal["name"]}',
            'data': first_part,
            'duration': float(len(first_part)) / first_signal['step'],
        })
        new_signal_entity = await insert(db.signals, first_signal)
        log.info(f'Signal(id={new_signal_entity["_id"]}, name={new_signal_entity["name"]}) created!')

    try:
        if data['partTwoSave']:
            second_signal = signal_data
            second_signal.update({
                'name': f'{signal["name"]} part II',
                'comment': f'{signal_comment}. Cut from {signal["name"]}',
                'data': second_part,
                'duration': float(len(second_part)) / second_signal['step'],
            })
            if data['partTwoOverride']:
                db.signals.find_one_and_update({'_id': ObjectId(signal_id)}, {'$set': second_signal})
            else:
                second_signal.update({'source': 'cutting'})
                new_signal_entity = await insert(db.signals, second_signal)
                log.info(f'Signal(id={new_signal_entity["_id"]}, name={new_signal_entity["name"]}) created!')
    except Exception as e:
        return

    if data['partThreeSave']:
        third_signal = deepcopy(signal_data)
        third_signal.update({
            'name': f'{signal["name"]} part III',
            'comment': f'{signal_comment}. Cut from {signal["name"]}',
            'data': third_part,
            'duration': float(len(third_part)) / third_signal['step'],
        })
        if data['partOneOverride']:
            db.signals.find_one_and_update({'_id': ObjectId(signal_id)}, {'$set': third_signal})
            db.signals.update_one({'_id': ObjectId(signal_id)}, {'$set': third_signal})
        else:
            third_signal.update({'source': 'cutting'})
            new_signal_entity = await insert(db.signals, third_signal)
            log.info(f'Signal(id={new_signal_entity["_id"]}, name={new_signal_entity["name"]}) created!')

    return web.json_response({'status': 200})


async def annotate(request):
    db = request.app['db']
    data = dict(request.json())
    signal_id = request.match_info.get('signal_id')
    signal = await db.signals.find_one({'_id': ObjectId(signal_id)})
    changes = {'annotations': deepcopy(signal.get('annotations', []))}
    changes['annotations'].append([
        data['x'],
        data['y'],
        data['annotation'],
    ])
    db.signals.find_one_and_update(
        {'_id': ObjectId(signal_id)},
        {'$set': changes},
    )
    return web.json_response({'status': 'OK'})


def annotate_interval(request):
    db = request.app['db']
    data = dict(request.json())
    signal_id = request.match_info.get('signal_id')
    signal = db.signals.find_one({'_id': ObjectId(signal_id)})
    changes = {'interval_annotation': signal.get('interval_annotation', [])}
    changes['interval_annotation'].append([
        data['from'],
        data['to'],
        data['annotation'],
    ])
    db.signals.find_one_and_update(
        {'_id': ObjectId(signal_id)},
        {'$set': changes},
    )
    return web.json_response({'status': 'OK'})
