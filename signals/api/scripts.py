import logging

from bson.objectid import ObjectId

from aiohttp import web

from signals.utils.db import to_list, insert, to_json
from signals.utils.script_executor import validate_str_function, apply_code_to_signal


log = logging.getLogger('app')


def find_scripts(request):
    db = request.app['db']
    data = to_list(db.scripts.find())
    return web.json_response(data)


async def create_script(request):
    data = dict(request.json())
    db = request.app['db']
    script = await insert(db.scripts, data)
    log.info(f'Script(id={script["_id"]}, name={script["name"]}) created!')
    return web.json_response(script)


async def update_script(request):
    db = request.app['db']
    data = dict(request.json())
    script_id = request.match_info.get('script_id')
    await db.scripts.find_one_and_update({'_id': ObjectId(script_id)}, {'$set': data})
    log.info(f'Script(id={script_id}, name={data["name"]}) updated!')
    return web.json_response({'status': 'OK'})


def validate(request):
    data = dict(request.json())
    errors = validate_str_function(data['code'])
    return web.json_response({
        'status': 'OK' if not errors else 'ERROR',
        'errors': errors,
    })


def add_comment_about_applying_script(comment, script, created=False):
    sufix = f'This signal was {"created" if created else "updated"} by applying Script({script["name"]})'
    return f'{comment}. {sufix}'


async def apply_script_to_signal(request):
    db = request.app['db']
    data = dict(request.json())
    log.info(data)
    signal_id = request.match_info.get('signal_id')
    script_id = data['script_id']
    signal = await db.signals.find_one({'_id': ObjectId(signal_id)})

    log.info(signal['name'])
    signal = to_json(signal)
    script = await db.scripts.find_one({'_id': ObjectId(script_id)})
    log.info(script)
    script = to_json(script)

    new_signal_data = apply_code_to_signal(script['code'], signal)

    new_comment = add_comment_about_applying_script(signal['comment'], script, not data.get('override_signal'))
    log.info(new_comment)
    duration = float(len(new_signal_data)) / signal['step']

    if data.get('override_signal'):
        db.signals.find_one_and_update(
            {'_id': ObjectId(signal_id)},
            {'$set': {
                'data': new_signal_data,
                'comment': new_comment,
                'duration': duration,
            }},
        )
    else:
        new_signal_entity = await insert(db.signals, {
            'name': f'{signal["name"]} MOD',
            'category_id': ObjectId(signal['category_id']),
            'data': new_signal_data,
            'step': data['step'],
            'comment': new_comment,
            'type': signal['type'],
            'duration': duration,
            'source': 'script',
        })
        log.info(f'Signal(id={new_signal_entity["_id"]}, name={new_signal_entity["name"]}) created!')
    return web.json_response({'status': 200})
