import trafaret as t


class ValidationError(Exception):
    pass


secrets_config_validator = t.Dict({
    'db': t.Dict({
        'port': t.Int,
        'db': t.String,
        'host': t.String,
        t.Key('user', optional=True): t.String,
        t.Key('password', optional=True): t.String,
    }),
})


def validate(data, validator, extra_message=None):
    try:
        return validator.check(data)
    except t.DataError as e:
        raise ValidationError(
            'Validation error: %s.\n%s' % (str(e.as_dict()), extra_message)
        )
