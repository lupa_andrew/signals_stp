import logging
from datetime import datetime
from copy import deepcopy


log = logging.getLogger('app')


def to_json(data):
    item = deepcopy(data)
    item.update({
        '_id': str(item['_id']),
        'created_at': item['created_at'].isoformat(),
    })
    if 'category_id' in item:
        item['category_id'] = str(item['category_id'])

    return item


async def to_list(cursor):
    results = []
    for item in cursor:
        results.append(to_json(item))
    return results


async def insert(collection, data):
    data.update({'created_at': datetime.now()})
    category = collection.insert_one(data)
    data.update({"_id": str(category.inserted_id)})
    return to_json(data)
