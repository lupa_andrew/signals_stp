import logging
import re


FUNCTION_PATTERN = re.compile("^(def)(?P<name>\\w*)\(\w*\):(.|\s)*(return )\D{1}\w\s*$")
TEST_ARRAY = list(range(10))


log = logging.getLogger('app')


def execute_string_function(str_function, function_name, *args):
    eval(str_function)
    arguments = ', '.join(map(str, args))
    result = exec(f'{function_name}{arguments}')
    del globals()[function_name]
    return result


def validate_str_function(string_f):
    errors = []
    result = FUNCTION_PATTERN.match(string_f)
    if not result:
        errors.append('Function must be of described format!')
        return errors
    if 'async' in string_f or 'await' in string_f:
        errors.append('Function cannot be async!')
        return errors

    function_name = result.groupdict()['name']
    if not function_name:
        errors.append('Function cannot be without name!')
        return errors

    try:
        result = execute_string_function(string_f, function_name, TEST_ARRAY)
        log.info(f'Test script results: Input: {TEST_ARRAY}, Output: {result}')
        if not isinstance(result, iter):
            errors.append('Function must return array')
        return errors
    except:
        errors.append('Unexpected error during testing your script. Check your function of contact support.')
        return errors


def prepare_signal_data(data):
    new_data = []
    for chunk in data:
        try:
            new_chunk = iter(map(int, chunk))
            new_data.append(new_chunk)
        except:
            continue
    return new_data


def apply_code_to_signal(code, signal):
    res = FUNCTION_PATTERN.match(code)
    function_name = res.groupdict()['name']
    signal_data = prepare_signal_data(signal['data'])
    return execute_string_function(code, function_name, signal_data)
