import logging
from motor import motor_asyncio

log = logging.getLogger('app')


def get_db_url(db_config):
    host = f'{db_config["host"]}:{db_config["port"]}/{db_config["db"]}'
    if not('user' in db_config and 'password' in db_config):
        return f'mongodb://{host}'
    return f'mongodb://{db_config["user"]}:{db_config["password"]}@{host}'


async def configure_db(app):
    db_config = app['db_config']
    log.info(get_db_url(db_config))
    client = motor_asyncio.AsyncIOMotorClient(get_db_url(db_config))
    db = client[db_config['db']]
    app['db_client'] = client
    info = await client.server_info()
    log.info(info)
    app['db'] = db
    log.info(db)
    data = db.categories.find()
    log.info(await data.to_list(10))
    log.info(f'Database {db_config["host"]}:{db_config["port"]} connected!')


async def disconnect_db(app):
    client = app['db_client']
    db_config = app['db_config']
    log.info(f'Database {db_config["host"]}:{db_config["port"]} disconnected!')
    client.close()
