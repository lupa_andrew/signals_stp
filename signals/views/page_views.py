import logging

import aiohttp_jinja2


log = logging.getLogger('app')


@aiohttp_jinja2.template('index.jinja2')
async def index(request):

    return {}
