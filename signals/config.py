import argparse
import yaml


from signals.validators import validate, secrets_config_validator


def make_parser():
    from os import environ
    ap = argparse.ArgumentParser(fromfile_prefix_chars='@')
    ap.add_argument('-b', '--host', default='localhost', metavar='HOSTNAME',
                    help="Bind to host (default: `%(default)s`)")
    ap.add_argument('-p', '--port', default=environ.get('PORT', 8080), type=int, metavar='PORT',
                    help="Bind to port (default: `%(default)s`)")
    ap.add_argument('-t', '--timeout', default=100, type=int, metavar='TIMEOUT',
                    help="Timeout in seconds (default: `%(default)s`)")
    ap.add_argument('--secrets', type=str, metavar='SECRETS',
                    help="Secret configs for database.")
    return ap


def get_config(path_to_secrets):
    with open(path_to_secrets) as file_stream:
        config = validate(
            yaml.load(file_stream),
            secrets_config_validator,
            'Wrong secrets in file %s' % path_to_secrets,
        )
        return config
