from signals.views import page_views
from signals.api import categories, signals, signal, scripts


def setup_routes(app):
    app.router.add_get('/', page_views.index)
    app.router.add_get('/categories', page_views.index)
    app.router.add_get('/categories/signals', page_views.index)
    app.router.add_get('/categories/signals/{category_id}', page_views.index)
    app.router.add_get('/signals/{signals_id}', page_views.index)
    app.router.add_get('/scripts', page_views.index)

    # categories
    app.router.add_post('/api/v1/categories/create', categories.create_category)
    app.router.add_get('/api/v1/categories', categories.find_categories)

    # signals
    app.router.add_get('/api/v1/signals', signals.find_signals)
    app.router.add_get('/api/v1/signals/{category_id}', signals.find_signals)
    app.router.add_post('/api/v1/signals/import', signals.import_signals)
    app.router.add_post('/api/v1/signals/import/check_db_connection', signals.check_db_connection)
    app.router.add_post('/api/v1/signals/import/load_from_db', signals.load_from_db)
    app.router.add_post('/api/v1/signals/import/import_from_db', signals.import_from_db)

    # actions with signal
    app.router.add_get('/api/v1/signal/{signal_id}', signal.find)
    app.router.add_delete('/api/v1/signal/{signal_id}/remove', signal.delete)
    app.router.add_post('/api/v1/signal/{signal_id}/update', signal.update)

    app.router.add_post('/api/v1/signal/{signal_id}/cut', signal.cut)
    app.router.add_post('/api/v1/signal/{signal_id}/annotate', signal.annotate)
    app.router.add_post('/api/v1/signal/{signal_id}/annotate_interval', signal.annotate_interval)

    app.router.add_post('/api/v1/signal/{signal_id}/apply', scripts.apply_script_to_signal)

    # scripts
    app.router.add_get('/api/v1/scripts', scripts.find_scripts)
    app.router.add_post('/api/v1/script', scripts.create_script)
    app.router.add_post('/api/v1/script/validate', scripts.validate)
    app.router.add_post('/api/v1/script/{script_id}', scripts.update_script)
