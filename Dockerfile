FROM python:3.6-alpine3.4

RUN apk update && apk add nodejs

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy files
ADD . /usr/src/app

RUN pip install -r requirements.txt

RUN npm i
#
#WORKDIR /usr/src/app/frontend
#RUN npm i
#
#WORKDIR /usr/src/app
#RUN node ./node_modules/webpack/bin/webpack.js --config webpack/config.js --bail --progress --colors
#RUN node ./node_modules/webpack/bin/webpack.js --config webpack/less.config.js --bail --progress --colors
#RUN rm -rf /usr/src/app/frontend
#RUN rm -rf /usr/src/app/node_modules

WORKDIR /usr/src/app/

ENTRYPOINT [ "python", "run.py", "--secrets", "./config/local.yaml", "--host", "0.0.0.0", "--port", "8080" ]
