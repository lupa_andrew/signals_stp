import request from 'superagent';
import _ from 'lodash';

import notify from 'components/notifications';


const allowedMethods = ['get', 'post', 'put'];

export default function fetch(rawMethod = '', url = '', obj = {}) {
    const method = String(rawMethod).toLowerCase();
    const exec = request[method];
    const headers = _.pick({}, ['X-CSRF-TOKEN']);
    if(!_.includes(allowedMethods, method)) {
        return Promise.reject(`Not valid method "${rawMethod}"`);
    }
    const handler = (data) => {
        if(!data.ok) throw data.body;
        return data.body;
    };
    if(method === 'get') {
        return exec(url)
            .set(headers)
            .query(obj)
            .then(handler);
    }
    if(method === 'delete') {
        return exec(url)
            .set(headers)
            .then(handler);
    }
    return exec(url)
        .type('json')
        .set(headers)
        .send(obj)
        .then(handler);
}


export class SafeFetch {

    static ERROR = 'ERROR';
    static defaultErrorHandler() {
        notify.error('Error', 'Unhandled error! Please, try again later');
    }

    constructor(errorHandler = SafeFetch.defaultErrorHandler) {
        this.errorHandler = errorHandler;
    }

    fetch(method, url = '', obj = {}) {
        return fetch(method, url, obj).catch((err) => {
            this.errorHandler(err);
            return SafeFetch.ERROR;
        });
    }

    get(url, obj) {
        return this.fetch('GET', url, obj);
    }

    put(url, obj) {
        return this.fetch('PUT', url, obj);
    }

    post(url, obj) {
        return this.fetch('POST', url, obj);
    }

    delete(url, obj) {
        return this.fetch('DELETE', url, obj);
    }

    static isError(result) {
        return result === SafeFetch.ERROR;
    }

}
