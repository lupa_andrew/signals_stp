import moment from 'moment';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'globalConstants';
import { isEmpty } from 'lodash';

import fetch, { SafeFetch } from './fetch';


const createReducer = (reduceObject, initialState) => {
    return (state = initialState, action) => {
        const executor = reduceObject[action.type];
        if(executor) return { ...state, ...executor(state, action.payload) };
        return state;
    };
};

const createAction = (type, payload = {}) => ({ type, payload });

const formatDate = str => moment(str).format(DATE_FORMAT);
const formatDateTime = str => moment(str).format(DATE_TIME_FORMAT);

const getErrorInfo = (errorText) => {
    if(isEmpty(errorText)) return {};
    return {
        validateStatus: 'error',
        help: errorText,
    };
};


export {
    fetch,
    createReducer,
    createAction,
    formatDate,
    formatDateTime,
    getErrorInfo,
};
