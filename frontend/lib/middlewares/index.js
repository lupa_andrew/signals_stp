import thunk from 'redux-thunk';
import createLogger from 'redux-logger';


const getTitleFormatter = component => (action, time, took) => {
    const parts = ['action'];
    parts.push(`%c${component}.${String(action.type)}`);
    if(time) parts.push(`%c@ ${time}`);
    if(took) parts.push(`(in ${took.toFixed(2)} ms)`);

    return parts.join(' ');
};


export default function getPageElementsMiddlewares(componentName) {
    const middlewares = [thunk];

    if(process.env.NODE_ENV !== 'cron' && process.env.NODE_ENV !== 'production') {
        const logger = createLogger({
            diff: true,
            titleFormatter: getTitleFormatter(componentName),
            collapsed: true,
        });
        middlewares.push(logger);
    }
    return middlewares;
}
