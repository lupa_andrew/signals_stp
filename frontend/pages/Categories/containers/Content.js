import React from 'react';
import PropTypes from 'prop-types';


import { Row } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions';

import CategoriesTable from '../components/CategoriesTable';


const HeaderContainer = ({
    categories,
}) => {
    return (
        <CategoriesTable
            categories={[]}
        />
    );
};

const mapStateToProps = state => ({
    categories: state.categories,
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
