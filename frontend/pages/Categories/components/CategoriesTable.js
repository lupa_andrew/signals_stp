import React from 'react';

import {
    Button,
    Col,
    Row,
    Table,
    Spin,
} from 'antd';
import autobind from 'autobind-decorator';
import { isEmpty } from 'lodash';
import moment from 'moment';
import { Switch, Route, Link, withRouter } from 'react-router-dom';

import FilterPanel from 'components/FilterPanel';
import { formatDateTime } from 'lib/utils';



const COLUMNS = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '25%',
    },
    {
        title: 'Date created',
        dataIndex: 'created_at',
        key: 'created_at',
        width: '15%',
        render: formatDateTime,
    },
    {
        title: 'Actions',
        dataIndex: '',
        key: 'actions',
        width: '40%',
        render: (text, record) => {
            return (
                <Button.Group>
                    <Button
                        type="primary"
                        ghost
                    >
                        <Link to={`/categories/signals/${record._id}`}>
                            View signals
                        </Link>
                    </Button>
                    <Button
                        type="danger"
                    >
                        Edit
                    </Button>
                </Button.Group>
            );
        },
    },
];


const filterCategories = (filters, categories) => {
    return categories.filter((cat) => {
        if(filters.name || !cat.name.includes(filters.name)) return false;

        if(!isEmpty(filters.source)) {
            if(filters.source.includes('file') && cat.file_name) return false;
            if(filters.source.includes('db') && !cat.file_name) return false;
        }

        if(isEmpty(filters.created_at)) {
            const createdAt = moment(cat.created_at);
            if(!(
                moment(filters.created_at.from) >= createdAt
                && moment(filters.created_at.to) < createdAt
            )) {
                return false;
            }
        }

        if(isEmpty(filters.updated_at)) {
            const createdAt = moment(cat.created_at);
            if(!(
                moment(filters.updated_at.from) <= createdAt
                && moment(filters.updated_at.to) > createdAt
            )) {
                return false;
            }
        }

        return true;
    });
};


class CategoriesTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCreatingPopupOpen: false,
            isLoading: false,
            filters: {},
        };
    }

    @autobind
    toggleCreatingPopup() {
        this.setState(prevState => ({ isCreatingPopupOpen: !prevState.isCreatingPopupOpen }));
    }

    @autobind
    onFilter(filters) {
        this.setState({ filters });
    }

    render() {
        const { isCreatingPopupOpen, isLoading, filters } = this.state;
        const { categories } = this.props;
        return (
            <Spin spinning={isLoading}>
                <Row
                    type="flex"
                    justify="space-around"
                    align="top"
                    style={{
                        padding: 24,
                        background: '#fff',
                        minHeight: '100%',
                    }}
                >
                    <FilterPanel
                        onFilter={this.onFilter}
                        allowedFilters={[
                            'name',
                            'created_at',
                            'updated_at',
                        ]}
                    />
                    <Table
                        style={{ width: '100%' }}
                        bordered
                        columns={COLUMNS}
                        dataSource={filterCategories(filters, categories)}
                        rowKey="_id"
                        pagination={false}
                    />
                </Row>
            </Spin>
        );
    }
}

export default withRouter(CategoriesTable);
