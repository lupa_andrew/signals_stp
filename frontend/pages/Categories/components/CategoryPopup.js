import React from 'react';

import autobind from 'autobind-decorator';
import { isEmpty } from 'lodash';
import { withRouter } from 'react-router-dom';

import {
    Button,
    Col,
    Form,
    Input,
    Modal,
    Row,
} from 'antd';


const closePopup = history => histiy.push('/categories');


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


class CategoryPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = { categoryName: '' };
    }

    @autobind
    onCreate() {
        const { categoryName } = this.state;
        if(isEmpty(categoryName)) return;
        this.props.onCreate(categoryName, this.props.onClose);
    }

    @autobind
    changeCategoryName({ target }) {
        this.setState(() => ({ categoryName: target.value }));
    }


    render() {
        const {
            onClose,
            visible,
        } = this.props;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                z-index="2000"
                closable={false}
                title={
                    <Row type="flex" justify="space-between">
                        <Col span={12}>
                            Create new Data set
                        </Col>
                        <Col span={12}>
                            <Row type="flex" justify="end">
                                <Button.Group
                                    <Button
                                        type="primary"
                                        onClick={this.onCreate}
                                    >
                                        Create
                                    </Button>
                                    <Button
                                        type="danger"
                                        ghost
                                        onClick={onClose}
                                    >
                                        Close
                                    </Button>
                                </Button.Group>
                            </Row>
                        </Col>
                    </Row>
                }
                visible={visible}
                footer={null}
            >
                <Row type="flex" justify="space-around" align="middle" style={{ minHeight: '200px' }}>
                    <Col span={18}>
                        <Form.Item
                            label="Data set name"
                            {...formItemLayout}
                        >
                            <Input
                                value={this.state.categoryName}
                                onChange={this.changeCategoryName}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Modal>
        );
    }
}


export default CategoryPopup;
