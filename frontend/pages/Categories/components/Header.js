import React from 'react';

import {
    Button,
    Col,
    Row,
} from 'antd';
import autobind from 'autobind-decorator';
import { Switch, Route, Link, withRouter } from 'react-router-dom';

import ImportPopup from 'components/ImportPopup';

import CategoryPopup from './CategoryPopup';



class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCreatingPopupOpen: false,
            isImportPopupOpen: false,
        };
    }

    @autobind
    toggleCreatingPopup() {
        this.setState(prevState => ({ isCreatingPopupOpen: !prevState.isCreatingPopupOpen }));
    }

    @autobind
    toggleImportPopup() {
        this.setState((prevState) => {
            if(!prevState.isImportPopupOpen) {
                return { isImportPopupOpen: !prevState.isImportPopupOpen };
            }
            window.location.reload();
        });
    }


    render() {
        const { isCreatingPopupOpen, isImportPopupOpen } = this.state;
        return (
            <Row type="flex" justify="space-around" align="middle" style={{height: '100%'}}>
                <CategoryPopup
                    visible={isCreatingPopupOpen}
                    onClose={this.toggleCreatingPopup}
                    onCreate={this.props.createNewCategory}
                />
                <ImportPopup
                    display={isImportPopupOpen}
                    onClose={this.toggleImportPopup}
                />
                <Col span={12}>
                    <Button.Group>
                        <Button
                            type="primary"
                            ghost
                            onClick={this.toggleCreatingPopup}
                        >
                            Create New Data set
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            onClick={this.toggleImportPopup}
                        >
                            Import Signals
                        </Button>
                    </Button.Group>
                </Col>
                <Col span={12}/>
            </Row>
        );
    }
}

export default Header;
