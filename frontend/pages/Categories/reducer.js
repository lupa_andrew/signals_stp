import { createReducer } from 'lib/utils';

import * as at from './actionTypes';


const initialState = {
    categories: [],
};


const reduceObject = {
    [at.ADD_CATEGORIES]: (state, { categories }) => ({
        categories: [...categories, ...state.categories],
    }),
};


export default createReducer(reduceObject, initialState);
