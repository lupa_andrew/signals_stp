import { SafeFetch, createAction } from 'lib/utils';

import * as at from './actionTypes';


const safeFetch = new SafeFetch();


const addCategories = categories => createAction(at.ADD_CATEGORIES, { categories });


export const createNewCategory = (categoryName, successCallback) => async (dispatch) => {
    const category = safeFetch.post('/api/v1/categories/create', { name: categoryName });
    if(SafeFetch.isError(category)) return;
    dispatch(addCategories([category]));
    successCallback();
};

export const fetchCategories = onLoadCallback => async (dispatch) => {
    const categories = safeFetch.get('/api/v1/categories');
    if(SafeFetch.isError(categories)) return;
    dispatch(addCategories(categories));
    onLoadCallback();
};
