import React from 'react';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { Layout, Menu, Spin, Row } from 'antd';
import autobind from 'autobind-decorator';

import getMiddlewares from 'lib/middlewares';
import BasicLayout from 'components/BasicLayout';

import { fetchCategories } from './actions';
import reducer from './reducer';
import Header from './containers/Header';
import Content from './containers/Content';


export default class Categories extends React.Component {

    constructor(props) {
        super(props);
        this.store = createStore(
            reducer,
            applyMiddleware(...getMiddlewares('CHATS')),
        );
        this.state = { isLoading: true };
    }

    componentDidMount() {
        this.store.dispatch(fetchCategories(this.stopLoading));
    }

    @autobind
    stopLoading() {
        this.setState(() => ({ isLoading: false }));
    }

    render() {
        return (
            <Provider store={this.store}>
                <BasicLayout header={<Header />}>
                    <Spin spinning={this.state.isLoading}>
                        <Content />
                    </Spin>
                </BasicLayout>
            </Provider>
        );
    }
}
