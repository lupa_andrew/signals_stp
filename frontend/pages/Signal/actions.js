import { SafeFetch, createAction } from 'lib/utils';

import * as at from './actionTypes';


const safeFetch = new SafeFetch();


const addSignal = signal => createAction(at.ADD_SIGNAL, { signal });


export const fetchSignal = (signalId, onLoadCallback) => async (dispatch) => {
    const signal = await safeFetch.get(`/api/v1/signal/${signalId}`);
    if(SafeFetch.isError(signal)) return;
    dispatch(addSignal(signal));
    onLoadCallback();
};
