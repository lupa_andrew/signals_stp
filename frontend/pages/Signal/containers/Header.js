import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions';

import Header from '../components/Header';


const HeaderContainer = ({
}) => {
    return (
        <Header />
    );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
