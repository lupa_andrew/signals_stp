import React from 'react';
import PropTypes from 'prop-types';


import { Row } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions';

import SignalStage from '../components/SignalStage';


const SignalContainer = ({
    signalId,
}) => {
    return (
        <SignalStage
            signalId={signalId}
        />
    );
};

export default SignalContainer;
// const mapStateToProps = state => ({
//     signal: state.signal,
// });
//
// const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
//
//
// export default connect(mapStateToProps, mapDispatchToProps)(SignalContainer);
