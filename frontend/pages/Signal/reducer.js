import { createReducer } from 'lib/utils';

import * as at from './actionTypes';


const initialState = {
    signal: {},
};


const reduceObject = {
    [at.ADD_SIGNAL]: (state, { signal }) => {
        console.log(signal);
        return ({
            signal,
        });
    },
};


export default createReducer(reduceObject, initialState);
