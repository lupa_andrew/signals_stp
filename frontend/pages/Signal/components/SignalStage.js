import React from 'react';

import ReactEcharts from 'echarts-for-react';

import {
    Button,
    Col,
    Row,
    Table,
    Divider,
    Select,
    Form,
} from 'antd';
import autobind from 'autobind-decorator';
import { isEmpty, chunk, isEqual, zip } from 'lodash';
import moment from 'moment';
import { Switch, Route, Link, withRouter } from 'react-router-dom';

import { SafeFetch } from 'lib/utils';


import {
    Line,
    LineChart,
    Area,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    ResponsiveContainer,
} from 'recharts';
import echarts from 'echarts';

import SignalStage from 'components/SignalStage';
import CategorySelect from 'components/CategorySelect';
import { formatDateTime } from 'lib/utils';


const safeFetch = new SafeFetch();

const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};



class SignalsStage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            signals: [],
            scripts: [],
            isLoading: false,
            signalIds: [props.signalId],
            maxDuration: 0,
        };
        this.charts = [React.createRef()];
        this.chartGroup = null;
    }

    componentDidMount() {
        this.fetchCategoriesAndSignals();
    }

    async fetchCategoriesAndSignals() {
        this.setState({ isLoading: true });
        const [categories, signals] = await Promise.all([
            safeFetch.get('/api/v1/categories'),
            safeFetch.get('/api/v1/signals'),
        ]);
        if(SafeFetch.isError(categories) || SafeFetch.isError(signals)) return;
        this.setState({
            categories,
            signals,
            isLoading: false,
        });
    }

    @autobind
    changeMaxDuration(length) {
        console.log(length);
        if(length < this.state.maxDuration) {
            this.setState({ maxDuration: length });
        }
    }

    @autobind
    addSignalToDisplay() {
        const { currentSignal } = this.state;
        this.charts = [...this.charts, React.createRef()];
        this.setState({
            currentSignal: null,
            currentCategory: null,
            signalIds: [...this.state.signalIds, currentSignal],
        });
    }

    renderCharts() {
        const { signalIds, maxDuration, categories } = this.state;
        this.chartsViews = zip(signalIds, this.charts).map(([signalId, ref]) => (
            <SignalStage
                signalId={signalId}
                ref={ref}
                key={signalId}
                maxDuration={maxDuration}
                changeMaxDuration={this.changeMaxDuration}
                categories={categories}
            />
        ));
        echarts.disconnect(this.chartGroup);
        return this.chartsViews;
    }

    componentDidUpdate() {
        if(this.chartsViews) {
            const a = echarts.connect(this.charts.map(c => {
                const chartInstance = c.current.chart.current.getEchartsInstance();
                return chartInstance;
            }));
        }
    }

    rememberClickDuration(x) {

    }

    render() {
        const { signalIds, categories, signals, currentCategory, currentSignal } = this.state;
        return (
            <div
                style={{
                    padding: 24,
                    background: '#fff',
                    minHeight: '100%',
                }}
            >
                <Row
                    type="flex"
                    justify="space-around"
                    align="top"
                >
                    <Col span={10}>
                        <Form.Item
                            {...formItemLayout}
                            label="Data set"
                        >
                            <CategorySelect
                                categories={categories}
                                value={currentCategory}
                                onChange={categoryId => this.setState({ currentCategory: categoryId })}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={10}>
                        <Form.Item
                            {...formItemLayout}
                            label="Signal"
                        >
                            <Select
                                disabled={!currentCategory}
                                value={currentSignal}
                                onChange={signalId => this.setState({ currentSignal: signalId })}
                                style={{ width: '100%' }}
                            >
                                {signals.filter(s => s.category_id === currentCategory).map(s => (
                                    <Select.Option
                                        value={s._id}
                                        key={s._id}
                                    >
                                        {s.name}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Button
                            onClick={this.addSignalToDisplay}
                            disabled={!currentSignal}
                        >
                            Add to display
                        </Button>
                    </Col>
                </Row>
                <Divider />
                {this.renderCharts()}
            </div>
        );
    }
}

export default withRouter(SignalsStage);
