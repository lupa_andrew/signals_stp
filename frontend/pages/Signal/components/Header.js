import React from 'react';

import {
    Button,
    Col,
    Row,
} from 'antd';
import autobind from 'autobind-decorator';
import { Switch, Route, Link, withRoute } from 'react-router-dom';

import ImportPopup from 'components/ImportPopup';


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isImportPopupOpen: false };
    }

    @autobind
    toggleImportPopup() {
        this.setState((prevState) => {
            if(!prevState.isImportPopupOpen) {
                return { isImportPopupOpen: !prevState.isImportPopupOpen };
            }
            window.location.load();
        });
    }

    render() {
        const { isImportPopupOpen } = this.state;
        return (
            <Row type="flex" justefy="space-around" align="middle" style={{ height: '100%' }}>
                <Col span={12} />
                <Col span={12} />
            </Row>
        );
    }
}

export default withRoute(Header);
