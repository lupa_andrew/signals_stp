import React from 'react';
import PropTypes from 'prop-types';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { Layout, Menu, Spin, Row } from 'antd';
import autobind from 'autobind-decorator';
import { withRouter } from 'react-router-dom';

import getMiddlewares from 'lib/middlewares';
import { SafeFetch, createAction } from 'lib/utils';

import BasicLayout from 'components/BasicLayout';

import { fetchSignal } from './actions';
import reducer from './reducer';
import Header from './containers/Header';
import Content from './components/SignalStage';


const safeFetch = new SafeFetch();


export default class Signal extends React.Component {

    static propTypes = {
        signalId: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.store = createStore(
            reducer,
            applyMiddleware(...getMiddlewares('SIGNAL')),
        );
        this.state = {
            isLoading: true,
            signals: {},
            signal: {},
        };
        this.signal = {};
    }


    render() {
        return (
            <Provider store={this.store}>
                <BasicLayout header={<Header />}>
                    <Content signalId={this.props.signalId} />
                </BasicLayout>
            </Provider>
        );
    }
}
