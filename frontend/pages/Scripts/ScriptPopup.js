import React from 'react';
import PropTypes from 'prop-types';

import autobind from 'autobind-decorator';
import { isEmpty } from 'lodash';
import { withRouter } from 'react-router-dom';
import { SafeFetch, getErrorInfo } from 'lib/utils';

import brace from 'brace';
import AceEditor from 'react-ace';


import {
    Button,
    Col,
    Form,
    Input,
    Modal,
    Row,
} from 'antd';


const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 18,
    },
};

const safeFetch = SafeFetch;


class ScriptPopup extends React.Component {

    static propTypes = {
        mode: PropTypes.oneOf(['create', 'update']),
        script: PropTypes.object,
    };

    static defaultProps = {
        script: {
            name: '',
            script: '',
            comment: '',
        },
    };

    static getDerivedStateFromProps(props) {
        return {
            errors: {},
            scriptError: '',
            scriptName: props.script.name,
            scriptComment: props.script.comment,
            scriptCode: props.script.code,
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            scriptName: props.script.name,
            scriptCode: props.script.code,
            scriptComment: props.script.comment,
            errors: {},
            scriptError: '',
        };
    }

    @autobind
    async onCreate() {
        const { scriptName, scriptCode, scriptComment } = this.state;
        const errors = {};
        if(isEmpty(scriptName)) errors.scriptName = 'This field is required';
        if(isEmpty(scriptCode)) errors.scriptCode = 'This field is required';
        this.setState({ errors });
        const isValidScript = await this.validateScript(scriptCode);
        if(!isValidScript || !isEmpty(errors)) return;
        const data = {
            name: scriptName,
            code: scriptCode,
            comment: scriptComment,
        };
        let result = null;
        if(this.props.mode === 'create') {
            result = await safeFetch.put('/api/v1/scripts', data);
        } else {
            result = await safeFetch.put(`/api/v1/scripts/${this.props.script._id}`, data);
        }
        if(SafeFetch.isError(result)) return;
        window.location.load();
    }

    @autobind
    changeScriptName({ target }) {
        this.setState(() => ({ scriptName: target.value }));
    }

    @autobind
    changeScriptComment({ target }) {
        this.setState(() => ({ scriptComment: target.value }));
    }

    @autobind
    onChangeScript(value) {
        this.setState({ scriptCode: value });
    }

    validateScript(code = '') {
        const result = safeFetch.post('/api/v1/script/validate', { code });
        let scriptError = 'Unexpected error';
        if(SafeFetch.isError(result)) {
            this.setState({ scriptError });
            return false;
        }
        scriptError = result.errors.join('. ');
        this.setState({ scriptError });
        return result.status === 'OK';
    }

    render() {
        const {
            onClose,
            visible,
            mode,
        } = this.props;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                z-index="2000"
                closable={false}
                title={
                    <Row type="flex" justify="space-between">
                        <Col span={12}>
                            {mode === 'create' ? 'Create new script' : 'Update script'}
                        </Col>
                        <Col span={12}>
                            <Row type="flex" justify="end">
                                <Button.Group>
                                    <Button
                                        type="primary"
                                        onClick={this.onCreate}
                                    >
                                        {mode === 'create' ? 'Create' : 'Update'}
                                    </Button>
                                    <Button
                                        type="danger"
                                        ghost
                                        onClick={onClose}
                                    >
                                        Close
                                    </Button>
                                </Button.Group>
                            </Row>
                        </Col>
                    </Row>
                }
                visible={visible}
                footer={null}
            >
                <Row type="flex" justify="space-around" align="middle" style={{ minHeight: '200px' }}>
                    <Col span={24}>
                        <Form.Item
                            label="Script name"
                            {...formItemLayout}
                            {...getErrorInfo(this.state.errors.scriptName)}
                        >
                            <Input
                                value={this.state.scriptName}
                                onChange={this.changeScriptName}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Script comment"
                            {...formItemLayout}
                            {...getErrorInfo(this.state.errors.scriptComment)}
                        >
                            <Input
                                value={this.state.scriptComment}
                                onChange={this.changeScriptComment}
                            />
                        </Form.Item>
                        <Row type="flex" justify="space-around" align="middle" style={{ margin: '10px 0' }}>
                            <Col offset={6} span={18}>
                                Script must be function, which accepts one iterable argument every element of which is array and returns the same!
                            </Col>
                        </Row>
                        {this.state.scriptError ? (
                            <Row type="flex" justify="space-around" align="middle" style={{ margin: '10px 0', color: 'red' }}>
                                <Col offset={6} span={18}>
                                    {this.state.scriptError}
                                </Col>
                            </Row>
                        ) : null}
                        <Form.Item
                            label="Script code"
                            {...formItemLayout}
                            {...getErrorInfo(this.state.errors.scriptCode)}
                        >
                            <AceEditor
                                mode="python"
                                theme="solarized_dark"
                                name="script"
                                width="100%"
                                // onLoad={this.onLoad}
                                onChange={this.onChangeScript}
                                fontSize={17}
                                showPrintMargin
                                showGutter
                                highlightActiveLine
                                value={this.state.scriptCode}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Modal>
        );
    }
}


export default ScriptPopup;
