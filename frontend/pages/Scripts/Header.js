import React from 'react';

import {
    Row,
    Col,
    Button,
} from 'antd';
import autobind from 'autobind-decorator';

import ApplyScriptPopup from 'components/ApplyScriptPopup';

import ScriptPopup from './ScriptPopup';


export default class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isCreatingPopupOpen: false,
            isApplyingPopupOpen: false,
        };
    }

    @autobind
    toggleCreatingPopup() {
        this.setState({ isCreatingPopupOpen: this.state.isCreatingPopupOpen });
    }

    @autobind
    toggleApplyingPopup() {
        this.setState({ isApplyingPopupOpen: this.state.isApplyingPopupOpen });
    }

    render() {
        const { isCreatingPopupOpen, isApplyingPopupOpen } = this.state;
        return (
            <Row type="flex" justify="space-around" align="middle" style={{ height: '100%' }}>
                <ScriptPopup
                    visible={isCreatingPopupOpen}
                    onClose={this.toggleCreatingPopup}
                    mode="create"
                />
                <ApplyScriptPopup
                    onClose={this.toggleApplyingPopup}
                />
                <Col span={12}>
                    <Button.Group>
                        <Button
                            type="primary"
                            ghost
                            onClick={this.toggleCreatingPopup}
                        >
                            Create New Script
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            onClick={this.toggleApplyingPopup}
                        >
                            Apply Script to signal
                        </Button>
                    </Button.Group>
                </Col>
                <Col span={12} />
            </Row>
        );
    }
}
