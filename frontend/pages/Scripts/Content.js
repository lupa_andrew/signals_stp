import React from 'react';

import {
    Row,
    Table,
    Button,
    Icon,
} from 'antd';
import autobind from 'autobind-decorator';
import _ from 'lodash';

import { formatDateTime } from 'lib/utils';

import ScriptPopup from './ScriptPopup';


const getColumns = (showScript, removeScript) => {
    return [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            width: '20%',
        },
        {
            title: 'Date created',
            dataIndex: 'created_at',
            key: 'created_at',
            width: '20%',
            render: formatDateTime,
        },
        {
            title: 'Comment',
            dataIndex: 'comment',
            key: 'comment',
            width: '40%',
        },
        {
            title: 'Actions',
            dataIndex: '',
            key: 'actions',
            width: '20%',
            render: (text, record) => {
                return (
                    <Button.Group>
                        <Button
                            type="primary"
                            ghost
                            onClick={() => showScript(record._id)}
                        >
                            View script
                        </Button>
                        <Button
                            type="danger"
                            onClick={() => removeScript(record._id)}
                        >
                            <Icon type="close" />
                        </Button>
                    </Button.Group>
                );
            },
        },
    ];
};


export default class Content extends React.Component {

    constructor(props) {
        super(props);
        this.state = { currentScript: null };
    }

    @autobind
    showScriptPopup(scriptId) {
        this.setState({ currentScript: scriptId });
    }

    @autobind
    closeScriptPopup() {
        this.setState({ currentScript: {} });
    }

    render() {
        const { scripts } = this.props;
        const { currentScript } = this.state;
        const visiblePopup = !_.isNull(currentScript);
        return (
            <Row
                type="flex"
                justify="space-around"
                align="top"
                style={{
                    padding: 24,
                    background: '#fff',
                    minHeight: '100%',
                }}
            >
                <ScriptPopup
                    visible={visiblePopup}
                    onClose={this.closeScriptPopup}
                    mode="update"
                    script={currentScript ? scripts.find(s => s._id === currentScript) : {}}
                />
                <Table
                    style={{ width: '100%' }}
                    size="small"
                    columns={getColumns(this.showScriptPopup, this.onRemoveScript)}
                    dataSource={scripts}
                    rowKey="_id"
                    pagination={false}
                />
            </Row>
        );
    }
}
