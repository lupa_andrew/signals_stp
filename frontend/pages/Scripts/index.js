import React from 'react';
import PropTypes from 'prop-types';

import { SafeFetch } from 'lib/utils';

import BasicLayout from 'components/BasicLayout';

import Header from './Header';
import Content from './Content';


const safeFetch = SafeFetch;


export default class Scripts extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            scripts: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.fetchScripts();
    }

    async fetchScripts() {
        this.setState({ isLoading: true });
        const scripts = await safeFetch.get('/api/v1/scripts');
        this.setState({ isLoading: false });
        if(SafeFetch.isError(scripts)) return;
        this.setState({ scripts });
    }

    render() {
        const { scripts } = this.state;
        return (
            <BasicLayout header={<Header />}>
                <Content scripts={scripts} />
            </BasicLayout>
        );
    }
}
