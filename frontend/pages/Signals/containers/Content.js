import React from 'react';
import PropTypes from 'prop-types';


import { Row } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions';

import SignalsTable from '../components/SignalsTable';


const HeaderContainer = ({
    signals,
    categories,
}) => {
    return (
        <SignalsTable
            signals={signals}
            categories={categories}
        />
    );
};

const mapStateToProps = state => ({
    signals: state.signals,
    categories: state.categories,
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
