import { SafeFetch, createAction } from 'lib/utils';

import * as at from './actionTypes';


const safeFetch = new SafeFetch();


const addSignals = signals => createAction(at.ADD_SIGNALS, { signals });
const addCategories = categories => createAction(at.ADD_CATEGORIES, { categories });


export const fetchSignals = (categoryId) => async (dispatch) => {
    const signals = await safeFetch.get(`/api/v1/signals${categoryId ? `${categoryId}` : ''}`);
    if(SafeFetch.isError(signals)) return;
    dispatch(addSignals(signals));
};


export const fetchCategories = onLoadCallback => dispatch => {
    const categories = safeFetch.get('/api/v1/categories');
    if(SafeFetch.isError(categories)) return;
    dispatch(addCategories(categories));
    onLoadCallback();
};
