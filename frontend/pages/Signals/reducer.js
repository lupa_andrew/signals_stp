import { createReducer } from 'lib/utils';

import * as at from './actionTypes';


const initialState = {
    signals: [],
    categories: [],
};


const reduceObject = {
    [at.ADD_SIGNALS]: (state, { signals }) => ({
        signals: [...signals],
    }),
    [at.ADD_CATEGORIES]: (state, { categories }) => ({
        categories,
    }),
};


export default createReducer(reduceObject, initialState);
