import React from 'react';

import {
    Button,
    Col,
    Icon,
    Row,
    Popover,
    Table,
    Tooltip,
} from 'antd';
import autobind from 'autobind-decorator';
import { isEmpty } from 'lodash';
import moment from 'moment';
import { Switch, Route, Link, withRouter } from 'react-router-dom';
import { SafeFetch } from 'lib/utils';

import FilterPanel from 'components/FilterPanel';
import { formatDateTime } from 'lib/utils';

import SignalPopup from './SignalPopup';


const safeFetch = SafeFetch;


const getColumns = (categories, showEditingPopup, removeSignal) => {
    return [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            width: '18%',
        },
        {
            title: 'Duration, sec',
            dataIndex: 'duration',
            key: 'duration',
            width: '9%',
            render: text => Math.round(text * 100) / 100,
        },
        {
            title: 'Source',
            dataIndex: 'source',
            key: 'source',
            width: '9%',
        },
        {
            title: 'Data set',
            key: 'category_id',
            dataIndex: 'category_id',
            width: '18%',
            render: (text) => {
                const category = categories.find(c => c._id === text);
                if(!category) return 'DELETED';
                return <Link to={`/categories/signals/${text}`}>{category.name}</Link>;
            },
        },
        {
            title: 'Type',
            key: 'type',
            dataIndex: 'type',
            width: '4%',
        },
        {
            title: <Tooltip title="Sampling frequency">SF</Tooltip>,
            key: 'step',
            dataIndex: 'step',
            width: '4%',
        },
        {
            title: 'Date created',
            dataIndex: 'created_at',
            key: 'created_at',
            width: '14%',
            render: formatDateTime,
        },
        {
            title: 'Comment',
            dataIndex: 'comment',
            key: 'comment',
            width: '15%',
        },
        {
            title: 'Actions',
            dataIndex: '',
            key: 'actions',
            width: '4%',
            render: (text, record) => {
                return (
                    <Popover
                        placement="leftTop"
                        content={
                            <React.Fragment>
                                <Row style={{ marginBottom: '10px' }}>
                                    <Button
                                        type="primary"
                                        ghost
                                        style={{ width: '100%' }}
                                    >
                                        <Link to={`/signals/${record._id}`}>
                                            View signal
                                        </Link>
                                    </Button>
                                </Row>
                                <Row style={{ marginBottom: '10px' }}>
                                    <Button
                                        type="danger"
                                        ghost
                                        style={{ width: '100%' }}
                                    >
                                        Export signal
                                    </Button>
                                </Row>
                                <Row style={{ marginBottom: '10px' }}>
                                    <Button
                                        type="danger"
                                        onClick={() => showEditingPopup(record._id)}
                                        style={{ width: '100%' }}
                                    >
                                        Edit
                                    </Button>
                                </Row>
                                <Row>
                                    <Button
                                        type="danger"
                                        onClick={() => removeSignal(record._id)}
                                        style={{ width: '100%' }}
                                    >
                                        <Icon type="close" />
                                    </Button>
                                </Row>
                            </React.Fragment>
                        }
                    >
                        <Button>Actions</Button>
                    </Popover>
                );
            },
        },
    ];
};


const filterSignals = (filters, signals) => {
    return signals.filter((cat) => {
        if(filters.name && !cat.name.includes(filters.name)) return false;

        if(!isEmpty(filters.source)) {
            if(filters.source.includes('file') && !cat.file_name) return false;
            if(filters.source.includes('db') && cat.file_name) return false;
        }

        if(!isEmpty(filters.created_at)) {
            const createdAt = moment(cat.created_at);
            if(!(
                moment(filters.created_at.from) <= createdAt
                && moment(filters.created_at.to) > createdAt
            )) {
                return false;
            }
        }

        if(!isEmpty(filters.updated_at)) {
            const createdAt = moment(cat.created_at);
            if(!(
                moment(filters.updated_at.from) <= createdAt
                && moment(filters.updated_at.to) > createdAt
            )) {
                return false;
            }
        }

        return true;
    });
};


class SignalsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCreatingPopupOpen: false,
            isEditingPopupOpen: false,
            currentEditingSignal: null,
            filters: [],
        };
    }

    @autobind
    toggleCreatingPopup() {
        this.setState(prevState => ({ isCreatingPopupOpen: !prevState.isCreatingPopupOpen }));
    }

    @autobind
    showEditingPopup(signalId) {
        this.setState({ isEditingPopupOpen: true, currentEditingSignal: signalId });
    }

    @autobind
    hideEditingPopup() {
        this.setState({ isEditingPopupOpen: false, currentEditingSignal: null });
    }

    onRemoveSignal(signalId) {
        const result = safeFetch.delete(`/api/v1/signal/${signalId}/delete`);
        if(SafeFetch.isError(result)) return;
        window.location.reload();
    }

    @autobind
    onFilter(filters) {
        this.setState({ filters });
    }

    render() {
        const { isEditingPopupOpen, filters, currentEditingSignal } = this.state;
        const { signals, categories } = this.props;
        return (
            <Row
                type="flex"
                justify="space-around"
                align="top"
                style={{
                    padding: 24,
                    background: '#fff',
                    minHeight: '100%',
                }}
            >
                <SignalPopup
                    visible={isEditingPopupOpen}
                    signal={signals.find(s => s._id === currentEditingSignal) || {}}
                    onClose={this.hideEditingPopup}
                    categories={categories}
                />
                <FilterPanel
                    onFilter={this.onFilter}
                    allowedFilters={[
                        'name',
                        'source',
                        'created_at',
                        'updated_at',
                        'categories',
                    ]}
                />
                <Table
                    style={{ width: '100%' }}
                    size="small"
                    columns={getColumns(categories, this.showEditingPopup, this.onRemoveSignal)}
                    dataSource={filterSignals(filters, signals)}
                    rowKey="_id"
                    pagination={false}
                />
            </Row>
        );
    }
}

export default SignalsTable;
