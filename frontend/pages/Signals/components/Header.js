import React from 'react';

import {
    Button,
    Col,
    Row,
} from 'antd';
import autobind from 'autobind-decorator';
import { Switch, Route, Link, withRouter } from 'react-router-dom';

import ImportPopup from 'components/ImportPopup';


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isImportPopupOpen: false };
    }

    @autobind
    toggleImportPopup() {
        this.setState(prevState => ({ isImportPopupOpen: prevState.isImportPopupOpen }));
    }

    render() {
        const { isImportPopupOpen } = this.state;
        return (
            <Row type="flex" justify="space-around" align="middle" style={{ height: '100%' }}>
                <ImportPopup
                    visible={isImportPopupOpen}
                    onClose={this.toggleImportPopup}
                />
                <Col span={12}>
                    <Button.Group>
                        <Button
                            type="primary"
                            ghost
                            onClick={this.toggleImportPopup}
                        >
                            Import signals
                        </Button>
                    </Button.Group>
                </Col>
                <Col span={12} />
            </Row>
        );
    }
}

export default withRouter(Header);
