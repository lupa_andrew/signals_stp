import React from 'react';

import autobind from 'autobind-decorator';
import { isEmpty, isEqual } from 'lodash';

import {
    Button,
    Col,
    Form,
    Input,
    Modal,
    Row,
    InputNumber,
} from 'antd';


import { SafeFetch, getErrorInfo } from 'lib/utils';

import CategorySelect from 'components/CategorySelect';
import SignalTypeSelect from 'components/SignalTypeSelect';

const closePopup = history => histiy.push('/categories');


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const safeFetch = new SafeFetch();


const getBasicState = (signal) => {
    return {
        name: signal.name,
        category_id: signal.category_id,
        type: signal.type || 'TEMP',
        step: signal.step || 500,
        comment: signal.comment,
    };
};


class SignalPopup extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ...getBasicState(props.signal),
            errors: {},
        };
    }

    static getDerivedStateFromProps(nextProps) {
        if(!isEqual(nextProps.signal)) {
            return {
                errors: {},
                ...getBasicState(nextProps.signal),
            };
        }
        return null;
    }

    @autobind
    async onCreate() {
        const { name, category_id, type, step, comment } = this.state;
        const errors = {};
        if(isEmpty(name)) errors.name = 'This field is required';
        if(isEmpty(category_id)) errors.category_id = 'This field is required';
        if(isEmpty(type)) errors.type = 'This field is required';
        if(!Number.isInteger(step)) errors.step = 'This field is required';
        this.setState({ errors });
        if(!isEmpty(errors)) return;

        const result = await safeFetch(`/api/v1/signal/${this.props.signal._id}/update`, {
            name, category_id, type, step,
        });
        if(SafeFetch.isError(result)) return;
        window.location.reload();
    }

    render() {
        const {
            visible,
            signal,
            categories,
        } = this.props;
        const { name, category_id, type, step, comment, errors } = this.state;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                z-index="2000"
                closable={false}
                title={
                    <Row type="flex" justify="space-between">
                        <Col span={12}>
                            {`Editing signal "${signal.name}"`}
                        </Col>
                        <Col span={12}>
                            <Row type="flex" justify="end">
                                <Button.Group>
                                    <Button
                                        type="primary"
                                        onClick={this.onCreate}
                                    >
                                        Update
                                    </Button>
                                    <Button
                                        type="danger"
                                        ghost
                                        onClick={closePopup}
                                    >
                                        Close
                                    </Button>
                                </Button.Group>
                            </Row>
                        </Col>
                    </Row>
                }
                visible={visible}
                footer={null}
            >
                <Row type="flex" justify="space-around" align="middle" style={{ minHeight: '200px' }}>
                    <Col span={18}>
                        <Form.Item
                            label="Name"
                            {...formItemLayout}
                            {...getErrorInfo(errors.name)}
                        >
                            <Input
                                value={name}
                                onChange={e => this.setState({ name: e.target.value })}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Data set"
                            {...formItemLayout}
                            {...getErrorInfo(errors.name)}
                        >
                            <CategorySelect
                                value={category_id}
                                categories={categories}
                                onChange={value => this.setState({ category_id: value })}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Signal type"
                            {...formItemLayout}
                            {...getErrorInfo(errors.type)}
                        >
                            <SignalTypeSelect
                                value={type}
                                onChange={value => this.setState({ type: value })}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Sampling frequency"
                            {...formItemLayout}
                            {...getErrorInfo(errors.step)}
                        >
                            <InputNumber
                                value={step}
                                min={1}
                                onChange={value => this.setState({ step: value })}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Comment"
                            {...formItemLayout}
                            {...getErrorInfo(errors.comment)}
                        >
                            <Input.TextArea
                                value={comment}
                                onChange={e => this.setState({ comment: e.target.value })}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Modal>
        );
    }
}


export default SignalPopup;
