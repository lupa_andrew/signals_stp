import React from 'react';
import PropTypes from 'prop-types';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { Layout, Menu, Spin, Row } from 'antd';
import autobind from 'autobind-decorator';
import { withRouter } from 'react-router-dom';

import getMiddlewares from 'lib/middlewares';
import BasicLayout from 'components/BasicLayout';

import { fetchSignals, fetchCategories } from './actions';
import reducer from './reducer';
import Header from './containers/Header';
import Content from './containers/Content';


export default class Signals extends React.Component {

    static propTypes = {
        categoryId: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.store = createStore(
            reducer,
            applyMiddleware(...getMiddlewares('SIGNALS')),
        );
        this.state = { isLoading: true };
    }

    componentDidMount() {
        this.fetchSignals();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.categoryId !== this.props.categoryId) this.fetchSignals();
    }

    @autobind
    fetchSignals() {
        const { categoryId } = this.props;
        this.store.dispatch(fetchSignals(categoryId, this.stopLoading));
    }

    @autobind
    fetchCategories() {
        this.store.dispatch(fetchCategories(() => {}));
    }

    @autobind
    stopLoading() {
        this.setState(() => ({ isLoading: false }));
    }

    render() {
        return (
            <Provider store={this.store}>
                <BasicLayout header={<Header />}>
                    <Spin spinning={this.state.isLoading}>
                        <Content />
                    </Spin>
                </BasicLayout>
            </Provider>
        );
    }
}
