import React from 'react';
import ReactDOM from 'react-dom';

import { Route, Redirect } from 'react-router-dom';

import PageSkeleton from 'components/PageSkeleton';

import Categories from './pages/Categories';
import Signals from './pages/Signals';
import Signal from './pages/Signal';
import Scripts from './pages/Scripts';


export default function () {
    ReactDOM.render(
        <PageSkeleton>
            <Route
                path="/categories/signals/:category_id?"
                render={({ match }) => {
                    const categoryId = match.param.category_id;
                    return <Signals categoryId={categoryId} />;
                }}
            />
            <Route
                path="/categories"
                component={Categories}
            />
            <Route
                path="/signals/:signal_id"
                render={({ match }) => {
                    const signalId = match.params.signal_id;
                    return <Signal signalId={signalId} />;
                }}
            />
            <Route
                path="/scripts"
                component={Scripts}
            />
            <Redirect to="/categories" />
        </PageSkeleton>,
        document.getElementById('index-page'),
    );
}
