import React from 'react';

import { Form, Checkbox} from 'antd';
import getErrorInfo from 'lib/utils';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const SOURCES = [
    {
        label: 'From file',
        value: 'file',
    },
    {
        label: 'From DB',
        value: 'db',
    },
    {
        label: 'From Script',
        value: 'script',
    },
    {
        label: 'By cutting',
        value: 'cutting',
    },
];

const BySource = ({
    value,
    onChange,
    error,
}) => {
    return (
        <Form.Item
            {...formItemLayout}
            {...getErrorInfo(error)}
            label="Source"
        >
            <Checkbox.Group
                value={value}
                options={SOURCES}
                onChange={onChange}
            />
        </Form.Item>
    );
};

export default BySource;
