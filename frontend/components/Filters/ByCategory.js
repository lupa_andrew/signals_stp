import React from 'react';

import { Form, Input} from 'antd';
import getErrorInfo from 'lib/utils';


const Option = Select.Option;
const safeFetch = new SafeFetch();

const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


class ByCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = { categories: [] };
    }
    componentDidMount() {
        this.fetchCategories();
    }

    async fetchCategories() {
        const categories = await safeFetch.get('/api/v1/categories');
        if(SafeFetch.isError(categories)) return;
        this.setState(() => ({ categories }));
    }

    render() {
        const {
            value,
            onChange,
            error,
        } = this.props;
        const { categories } = this.state;
        return (
            <Form.Item
                {...formItemLayout}
                {...getErrorInfo(error)}
                label="Data sets"
            >
                <Select
                    mode="multiple"
                    value={value}
                    onChange={onChange}
                    style={{ width: '100%' }}
                >
                    {categories.map(category => (
                        <Option
                            value={category._id}
                            key={category._id}
                        >
                            {category.name}
                        </Option>
                    ))}
                </Select>
            </Form.Item>
        );
    }
}

export default ByCategory;
