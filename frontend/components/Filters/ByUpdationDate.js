import React from 'react';

import { Form, DatePicker } from 'antd';
import moment from 'moment';

import getErrorInfo from 'lib/utils';

const { RangePicker } = DatePicker;

const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const ByUpdationDate = ({
    value,
    onChange,
    error,
}) => {
    return (
        <Form.Item
            {...formItemLayout}
            {...getErrorInfo(error)}
            label="Date updated"
        >
            <RangePicker
                defaultValue={[moment(), moment()]}
                value={value}
                onChange={onChange}
                showTime={{
                    format: 'HH:mm',
                }}
                style={{ width: '100%' }}
            />
        </Form.Item>
    );
};

export default ByUpdationDate;
