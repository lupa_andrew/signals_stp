import React from 'react';

import { Form, DatePicker} from 'antd';
import getErrorInfo from 'lib/utils';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const ByCreationDate = ({
    value,
    onChange,
    error,
}) => {
    return (
        <Form.Item
            {...formItemLayout}
            {...getErrorInfo(error)}
            label="Date created"
        >
            <DatePicker
                defaultValue={[moment(), moment()]}
                value={value}
                onChange={onChange}
                showTime={{
                    format: 'HH:mm',
                }}
                style={{ width: '100%' }}
            />
        </Form.Item>
    );
};

export default ByCreationDate;
