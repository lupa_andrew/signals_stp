import React from 'react';

import { Form, Input} from 'antd';
import { getErrorInfo } from 'lib/utils';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const ByName = ({
    value,
    onChange,
    error,
}) => {
    return (
        <Form.Item
            {...formItemLayout}
            {...getErrorInfo(error)}
            label="Name"
        >
            <Input
                value={value}
                onChange={onChange}
            />
        </Form.Item>
    );
};

export default ByName;
