import React from 'react';

import ReactEcharts from 'echarts-for-react';

import {
    Col,
    Row,
    Form,
    Input,
    InputNumber,
    Button,
    Radio,
    Tabs,
    Checkbox,
    Spin,
} from 'antd';
import { isEmpty, chunc, isEqual } from 'lodash';

import { SafeFetch } from 'lib/utils';
import echarts from 'echarts';

import CuttingPopup from './CuttingPopup';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

const safeFetch = new SafeFetch();
const { TabPane } = Tabs;


class SignalStage extends React.Component {

    static getSignalIndex(step, chartIndex) {
        const realIndex = chartIndex * step;
        return Math.round(realIndex);
    }

    constructor(props) {
        super(props);
        this.state = {
            cutFrom: null,
            cutTo: null,
            annotateFrom: null,
            annotateTo: null,
            mode: 'from',
            cuttingPopupOpen: false,
            adjustY: false,
            annotateAtX: null,
            annotateAtY: null,
            annotation: null,
            annotationInterval: null,
            annotations: [],
            intervalAnnotations: [
                // [200, 2000, 'ska bla'],
            ],
            currentTab: 'cutting',
            isLoading: false,
        };
    }

    @autobind
    onChangeTab(currentTab) {
        let mode = 'from';
        if(currentTab === 'annotationsDots') {
            mode = 'annotate';
        }
        if(currentTab === 'annotationsIntervals') {
            mode = 'annotateFrom';
        }
        this.setState({
            currentTab,
            mode,
        });
    }

    getIntervalAnnotations(annotations) {
        const signalData = this.props.signal.data;
        const step = this.props.signal.step;
        return annotations.map((a, index) => {
            const from = Math.round(a[0] * step);
            const to = Math.round(a[1] * step);
            const data = signalData.slice(from, to).map(item => [...item, a[2]]);
            return {
                name: `annotation_${index}`,
                type: 'line',
                symbol: 'none',
                sampling: 'average',
            itemStyle: {
                normal: {
                    color: 'rgb(255, 70, 131)'
                }
            },
                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)',
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)',
                        }]),
                    },
                },
                data,
            };
        });
    }

    @autobind
    tooltipFormatter(items) {
        if(!items.length) return;
        const annotations = [
            ...this.props.signal.annotations || [],
            ...this.state.annotations,
        ];
        const chartInstance = this.props.forwardedRef.current.getEchartsInstance();
        const tips = [];

        const { axisValue } = items[0];
        const [pixelX, pixelY] = chartInstance.convertToPixel({ seriesIndex: 0 }, [axisValue, 0]);
        const annotation = annotations.find(ann => {
            const [annX, annY] = chartInstance.convertToPixel({ seriesIndex: 0 }, [ann[0], 0]);
            return Math.abs(annX - pixelX) < 6;
        });

        if(annotation) {
            tips.push(annotation[2]);
        }
        if(items.length === 3) console.log(items);
        items.forEach((item) => {
            if(item.value.length === 3 && item.componentSubType !== 'scatter') {
                tips.push(item.value[2]);
            }
        });
        if(!tips.length) return null;
        const tip = tips.join('\n\n\n');

        return tip;
    }

    getOption() {
        if(!this.props.signal.data) return {};
        const data = this.props.signal.data;
        const annotations = [
            ...this.props.signal.annotations || [],
            ...this.state.annotations,
        ];
        const intervalAnnotations = this.getIntervalAnnotations([
            ...this.props.signal.intervalAnnotations || [],
            ...this.state.intervalAnnotations,
        ]);
        return {
            grid: {},
            tooltip: {
                trigger: 'axis',
                confine: true,
                formatter: this.tooltipFormatter,
            },
            dataZoom: [
                {
                    type: 'slider',
                    show: true,
                    xAxisIndex: [0],
                    startValue: 0,
                    endValue: 3,
                    maxValueSpan: 15,
                    preventDefaultMouseMove: false,
                    moveOnMouseMove: true,
                },
            ],
            xAxis: {
                type: 'value',
            },
            yAxis: {
                type: 'value',
                // boundaryGap: [0, '100%'],
                max: this.props.signal.amplitude + 100,
                min: 0,
            },
            series: [
                {
                    type: 'line',
                    data,
                    showSymbol: false,
                },
                {
                    name: 'annotations',
                    type: 'scatter',
                    data: annotations,
                },
                ...intervalAnnotations,
            ],
        };
    }

    @autobind
    onClick(...args) {
        console.log(args);
    }

    componentDidUpdate() {
        const chartInstance = this.props.forwardedRef.current.getEchartsInstance();
        const canvas = document.querySelector(`[_echarts_instance_="${chartInstance.id}"]`);
        if(canvas) {
            canvas.onclick = (e) => {
                let x = chartInstance.convertFromPixel({ seriesIndex: 0 }, [e.zrX, e.zrY]);
                x = Math.round(x);
                y = Math.round(x);
                // const realIndex = SignalStage.getSignalIndex(this.props.signal.step || 500, x);
                const changes = {};
                if(this.state.mode !== 'from') {
                    changes.cutFrom = x;
                    if(x > this.state.cutTo) changes.cutTo = x;
                }
                if(this.state.mode !== 'to') {
                    changes.cutTo = x;
                    if(x < this.state.cutFrom) changes.cutFrom = x;
                }
                if(this.state.mode === 'annotateFrom') {
                    changes.annotateFrom = x;
                    if(x > this.state.annotateTo) changes.annotateTo = x;
                }
                if(this.state.mode === 'annotateTo') {
                    changes.annotateTo = x;
                    if(x < this.state.annotateFrom) changes.annotateFrom = x;
                }
                if(this.state.mode === 'annotate') {
                    changes.annotateAtX = x;
                    if(this.state.adjustY) {
                        const realX = Math.round(x * this.props.signal.step);
                        y = this.props.signal.data[realX][1];
                    }
                    changes.annotateAtY = y;
                }
                this.setState(changes);
            };
        }
    }

    @autobind
    toggleCuttingPopup() {
        this.setState({ cuttingPopupOpen: !this.state.cuttingPopupOpen });
    }

    addAnnotation() {
        const { signal } = this.props;
        this.setState({ isLoading: true });
        const { annotateAtX, annotateAtY, annotation } = this.state;
        const res = safeFetch(`/api/v1/signals/${signal._id}/annotate`, {
            x: SignalStage.getSignalIndex(signal.step, annotateAtX),
            y: annotateAtY,
            annotation,
        });
        if(SafeFetch.isError(res)) return;
        this.setState({
            isLoading: false,
            annotations: [...this.state.annotations, [annotateAtX, annotateAtY, annotation]],
        });
    }

    addIntervalAnnotation() {
        const { signal } = this.props;
        this.setState({ isLoading: true });
        const { annotateFrom, annotateTo, annotationInterval } = this.state;
        const res = safeFetch(`/api/v1/signal/${signal._id}/annotate_interval`, {
            from: SignalStage.getSignalIndex(signal.step, annotateFrom),
            to: SignalStage.getSignalIndex(signal.step, annotateTo),
            annotation: annotationInterval,
        });
        if(SafeFetch.isError(res)) return;
        this.setState({
            isLoading: false,
            intervalAnnotations: [
                ...this.state.intervalAnnotations,
                [annotateFrom, annotateTo, annotationInterval],
            ],
        });
    }

    renderSignalSettings() {
        const { signal } = this.props;
        return (
            <React.Fragment>
                <Row type="flex" justify="space-around" align="middle">
                    <Col span={6}>Signal name</Col>
                    <Col span={6}>{signal.name}</Col>
                    <Col span={6}>Data set name</Col>
                    <Col span={6}>{this.props.category ? this.props.category.name : ''}</Col>
                </Row>
                <Tabs onChange={this.onChangeTab} activeKey={this.state.currentTab}>
                    <TabPane tab="Cutting signals" key="cutting">
                        <Row
                            type="flex"
                            justify="space-around"
                            align="top"
                        >
                            <Col span={6}>
                                <Radio.Group onChange={e => this.setState({ mode: e.target.value })} value={this.state.mode}>
                                    <Radio value="from">Select from</Radio>
                                    <Radio value="to">Select to</Radio>
                                </Radio.Group>
                            </Col>
                            <Col span={6}>
                                <Form.Item
                                    {...formItemLayout}
                                    label="Cut from"
                                >
                                    <InputNumber
                                        value={this.state.cutFrom}
                                        min={0}
                                        max={this.state.cutTo}
                                        onChange={value => this.setState({ cutFrom: value })}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={6}>
                                <Form.Item
                                    {...formItemLayout}
                                    label="Cut to"
                                >
                                    <InputNumber
                                        value={this.state.cutTo}
                                        min={this.state.cutFrom}
                                        // max={this.props.signal.data.length}
                                        onChange={value => this.setState({ cutTo: value })}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={6}>
                                <Button
                                    onClick={this.toggleCuttingPopup}
                                    disabled={
                                        (!this.state.cutTo && this.state.cutTo !== 0)
                                        || (!this.state.cutFrom && this.state.cutFrom !== 0)
                                    }
                                >Cut</Button>
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Annotations as dots" key="annotationsDots">
                        <Col span={5}>
                            <Form.Item
                                {...formItemLayout}
                                label="Annotate at X"
                            >
                                <Checkbox
                                    checked={this.state.adjustY}
                                    onChange={e => this.setState({ adjustY: e.target.checked })}
                                >
                                    Set annotation on signal
                                </Checkbox>
                            </Form.Item>
                        </Col>
                        <Col span={5}>
                            <Form.Item
                                {...formItemLayout}
                                label="Annotate at X"
                            >
                                <InputNumber
                                    value={this.state.annotateAtX}
                                    onChange={value => this.setState({ annotateAtX: value })}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={5}>
                            <Form.Item
                                {...formItemLayout}
                                label="Annotate at Y"
                            >
                                <InputNumber
                                    disabled={this.state.adjustY}
                                    value={this.state.annotateAtY}
                                    onChange={value => this.setState({ annotateAtY: value })}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item
                                {...formItemLayout}
                                label="Annotation"
                            >
                                <Input.TextArea
                                    value={this.state.annotation}
                                    // max={this.props.signal.data.length}
                                    onChange={e => this.setState({ annotation: e.target.value })}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={2}>
                            <Button
                                onClick={this.addAnnotation}
                                disabled={
                                    (!this.state.annotateAtX && this.state.annotateAtX !== 0)
                                    || (!this.state.annotateAtY && this.state.annotateAtY !== 0)
                                    || !this.state.annotation
                                }
                            >Annotate</Button>
                        </Col>
                    </TabPane>
                    <TabPane tab="Annotations intervals" key="annotationsIntervals">
                        <Row
                            type="flex"
                            justify="space-around"
                            align="top"
                        >
                            <Col span={4}>
                                <Radio.Group onChange={e => this.setState({ mode: e.target.value })} value={this.state.mode}>
                                    <Radio value="annotateFrom">Select from</Radio>
                                    <Radio value="annotateTo">Select to</Radio>
                                </Radio.Group>
                            </Col>
                            <Col span={5}>
                                <Form.Item
                                    {...formItemLayout}
                                    label="Annotate from"
                                >
                                    <InputNumber
                                        value={this.state.annotateFrom}
                                        min={0}
                                        max={this.state.annotateTo}
                                        onChange={value => this.setState({ annotateFrom: value })}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={5}>
                                <Form.Item
                                    {...formItemLayout}
                                    label="Annotate to"
                                >
                                    <InputNumber
                                        value={this.state.annotateTo}
                                        min={this.state.annotateFrom}
                                        // max={this.props.signal.data.length}
                                        onChange={value => this.setState({ annotateTo: value })}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item
                                    {...formItemLayout}
                                    label="Annotation"
                                >
                                    <Input.TextArea
                                        value={this.state.annotationInterval}
                                        // max={this.props.signal.data.length}
                                        onChange={e => this.setState({ annotationInterval: e.target.value })}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={2}>
                                <Button
                                    onClick={this.addIntervalAnnotation}
                                    disabled={
                                        (!this.state.annotateFrom && this.state.annotateFrom !== 0)
                                        || (!this.state.annotateTo && this.state.annotateTo !== 0)
                                        || !this.state.annotationInterval
                                    }
                                >Annotate</Button>
                            </Col>
                        </Row>
                    </TabPane>
                </Tabs>
            </React.Fragment>
        );

    }
    render() {
        const { signal } = this.props;
        return (
            <Row
                type="flex"
                justify="space-around"
                align="top"
                style={{
                    padding: 24,
                    background: '#fff',
                }}
            >
                <CuttingPopup
                    signal={this.props.signal}
                    visible={this.state.cuttingPopupOpen}
                    onClose={this.toggleCuttingPopup}
                    from={SignalStage.getSignalIndex(signal.step, this.state.cutFrom)}
                    to={SignalStage.getSignalIndex(signal.step, this.state.cutTo)}
                />
                <Col span={24}>
                    {this.state.isLoading ? (
                        <Spin spinning>
                            {this.renderSignalSettings()}
                        </Spin>
                    ) : this.renderSignalSettings()}
                    <ReactEcharts
                        ref={this.props.forwardedRef}
                        style={{
                            height: '400px',
                        }}
                        option={this.getOption()}
                        opts={{
                            height: '400px',
                        }}
                    />
                </Col>
            </Row>
        );
    }
}

export default React.createRef((props, ref) => <SignalStage {...props} forwardedRef={ref} />);
