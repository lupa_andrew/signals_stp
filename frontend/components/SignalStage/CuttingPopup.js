import React from 'react';
import PropTypes from 'prop-types';

import {
    Modal,
    Row,
    Col,
    Form,
    Checkbox,
    Select,
    Button,
    Spin,
    CheckBox,
} from 'antd';
import autobind from 'autobind-decorator';
import { isEmpty } from 'lodash';

import { SafeFetch, getErrorInfo } from 'lib/utils';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const safeFetch = new SafeFetch();


export default class ApplyScriptPopup extends React.Component {

    static propTypes = {
        onClose: PropTypes.func,
        visible: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            partOneSave: false,
            partOneOverride: false,
            partTwoSave: true,
            partTwoOverride: true,
            partThreeSave: false,
            partThreeOverride: false,
        };
    }

    // componentDidMount() {
    //     this.fetchData();
    // }
    //
    // async fetchData() {
    //     this.setState({ isLoading: true });
    //     const [categories, signals, scripts] = await Promise.all([
    //         safeFetch.get('/api/v1/categories'),
    //         safeFetch.get('/api/v1/signals'),
    //         safeFetch.get('/api/v1/scripts'),
    //     ]);
    //     if(
    //         SafeFetch.isError(categories)
    //         || SafeFetch.isError(signals)
    //         || SafeFetch.isError(scripts)
    //     ) return;
    //     this.setState({
    //         categories,
    //         signals,
    //         scripts,
    //         isLoading: false,
    //     });
    // }

    onApply() {
        const errors = {};
        const { signalId, categoryId, scriptId, overrideSignal } = this.state;
        if(!categoryId) errors.categoryId = 'This field is required';
        if(!signalId) errors.signalId = 'This field is required';
        if(!scriptId) errors.scriptId = 'This field is required';
        this.setState({ errors });
        const res = safeFetch.post(`/api/v1/signal/${signalId}/apply`, {
            script_id: scriptId,
            override_signal: overrideSignal,
        });
        if(SafeFetch.isError(res)) return;
        window.location.load();
    }

    render() {
        const { onClose, visible, signal, from, to } = this.props;
        const {
            errors,
            isLoading,
            partOneSave,
            partOneOverride,
            partTwoSave,
            partTwoOverride,
            partThreeSave,
            partThreeOverride,
        } = this.state;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                z-index="2000"
                closable={false}
                title={
                    <Spin spinning={isLoading}>
                        <Row type="flex" justify="space-between">
                            <Col span={12}>
                                {`Cutting signal ${signal.name}`}
                            </Col>
                            <Col span={12}>
                                <Row type="flex" justify="end">
                                    <Button.Group>
                                        <Button
                                            type="primary"
                                            onClick={this.onApply}
                                        >
                                            Cut
                                        </Button>
                                        <Button
                                            type="danger"
                                            ghost
                                            onClick={onClose}
                                        >
                                            Close
                                        </Button>
                                    </Button.Group>
                                </Row>
                            </Col>
                        </Row>
                    </Spin>
                }
                visible={visible}
                footer={null}
            >
                <Spin spinning={isLoading}>
                    <Row type="flex" justify="space-around" align="middle" style={{ minHeight: '200px' }}>
                        <Col span={18}>
                            <Form.Item
                                label={`Part I: from ${0} to ${from}`}
                                {...formItemLayout}
                            >
                                <Checkbox
                                    checked={partOneSave}
                                    onChange={e => this.setState({ partOneSave: e.target.checked })}
                                >
                                    Save
                                </Checkbox>
                                <Checkbox
                                    disabeled={!partOneSave}
                                    checked={partOneOverride}
                                    onChange={e => this.setState({
                                        partOneOverride: e.target.checked,
                                        partTwoOverride: false,
                                        partThreeOverride: false,
                                    })}
                                >
                                    Override existing
                                </Checkbox>
                            </Form.Item>
                            <Form.Item
                                label={`Part II: from ${from} to ${to}`}
                                {...formItemLayout}
                            >
                                <Checkbox
                                    checked={partTwoSave}
                                    onChange={e => this.setState({ partTwoSave: e.target.checked })}
                                >
                                    Save
                                </Checkbox>
                                <Checkbox
                                    disabeled={!partTwoSave}
                                    checked={partTwoOverride}
                                    onChange={e => this.setState({
                                        partOneOverride: false,
                                        partTwoOverride: e.target.checked,
                                        partThreeOverride: false,
                                    })}
                                >
                                    Override existing
                                </Checkbox>
                            </Form.Item>
                            <Form.Item
                                label={`Part III: from ${to} to ${signal.realLength}`}
                                {...formItemLayout}
                            >
                                <Checkbox
                                    checked={partThreeSave}
                                    onChange={e => this.setState({ partThreeSave: e.target.checked })}
                                >
                                    Save
                                </Checkbox>
                                <Checkbox
                                    disabeled={!partThreeSave}
                                    checked={partThreeOverride}
                                    onChange={e => this.setState({
                                        partOneOverride: false,
                                        partTwoOverride: false,
                                        partThreeOverride: e.target.checked,
                                    })}
                                >
                                    Override existing
                                </Checkbox>
                            </Form.Item>
                        </Col>
                    </Row>
                </Spin>
            </Modal>
        );
    }
}