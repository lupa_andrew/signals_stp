import React from 'react';
import PropTypes from 'prop-types';


import { Layout, Menu, Spin, Row } from 'antd';
import autobind from 'autobind-decorator';
import { map } from 'lodash';

import { SafeFetch } from 'lib/utils';

import Content from './Content';


const safeFetch = SafeFetch;


export default class Signal extends React.Component {

    static propTypes = {
        signalId: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            signals: {},
            signal: {},
        };
        this.chart = React.createRef();
    }

    componentDidMount() {
        this.fetchSignal();
    }

    static addEmptyPart(data, step, maxDuration) {
        const newData = [...data];
        let currentDuration = data.length / step;
        while (currentDuration < maxDuration) {
            currentDuration += 1 / step;
            newData.push([currentDuration, 0]);
        }
        return newData;
    }
    componentDidUpdate(prevProps) {
        if(prevProps.maxDuration !== this.props.maxDuration && this.state.signal.data) {
            const step = this.state.signal.step || 500;
            this.setState({
                signal: {
                    ...this.state.signal,
                    data: Signal.addEmptyPart(this.state.signal.data, step, this.props.maxDuration),
                },
            });
        }
    }

    @autobind
    async fetchSignal() {
        const { signalId } = this.props;
        const signal = await safeFetch.get(`/api/v1/signal/${signalId}`);
        if(SafeFetch.isError(signal)) return;
        const amplitude = signal.data.reduce((acc, s) => {
            if(parseFloat(s[0]) > acc) return parseFloat(s[2]);
            return acc;
        });
        const step = signal.step || 500;
        const currentDuration = (signal.data.length * step) / 1000;
        const data = signal.data.map((s, index) => [(index * step) / 1000, parseFloat(s[2])]);
        const realLength = data.length;
        if(this.props.maxDuration < currentDuration) this.props.changeMaxDuration(currentDuration);
        this.setState({
            signal: {
                ...signal,
                data: Signal.addEmptyPart(data, step, this.props.maxDuration),
                amplitude,
                realLength,
                annotations,
                intervalAnnotations,
            },
        });
        this.stopLoading();
    }

    @autobind
    stopLoading() {
        this.setState(() => ({ isLoading: false }));
    }

    render() {
        const category = this.props.categories.find(c => c._id === this.state.signal.category_id);
        const graph = <Content signal={this.state.signal} ref={this.chart} category={category} />;
        // if(this.state.isLoading) {
        return (
            <Spin spinning={this.state.isLoading}>
                {graph}
            </Spin>
        );
        // }
        // return graph;
    }
}

// export default React.forwardRef((props, ref) => <Signal forwardedRef={ref} {...props} />);
