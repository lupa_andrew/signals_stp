import React from 'react';

import { notification, Icon } from 'antd';


const basicConfig = {
    duration: 3,
    icon: <Icon type="check-circle" style={{ color: 'green' }} />,
    placement: 'bottomRight',
};

export default {
    error: (message, description) => {
        notification.error({
            ...basicConfig,
            icon: <Icon type="closcircle" style={{ color: 'red' }} />,
            message,
            description,
        });
    },
    success: (message, description) => {
        notification.success({
            ...basicConfig,
            message,
            description,
        });
    },
    info: (message, description) => {
        notification.success({
            ...basicConfig,
            message,
            description,
        });
    },
};
