import React from 'react';
import PropTypes from 'prop-types';

import { Router, Route, Switch, Redirect, Link } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

import { Layout, Menu, Icon, Row } from 'antd';
import autobind from 'autobind-decorator';

const { Header, Sider, Content } = Layout;


export default class BasicLayout extends React.Component {

    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.element),
            PropTypes.element,
        ]),
        header: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.element),
            PropTypes.element,
        ]),
    };

    render() {
        return (
            <Layout style={{ height: '100%', minHeight: '1px', maxWidth: 'calc(100vw - 200px)' }}>
                <Header style={{ background: '#fff', marginBottom: 16 }}>
                    {this.props.header}
                </Header>
                <Content
                    style={{
                        overflowY: 'auto',
                        padding: '0 16px 16px 16px',
                        height: '100%',
                    }}
                >
                    {this.props.children}
                </Content>
            </Layout>
        );
    }
}
