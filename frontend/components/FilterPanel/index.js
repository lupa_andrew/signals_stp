import React from 'react';

import {
    Button,
    Popover,
    Row,
    Col,
} from 'antd';
import autobind from 'autobind-decorator';

import ByName from 'components/Filters/ByName';
import ByCreationDate from 'components/Filters/ByCreationDate';
import ByUpdationDate from 'components/Filters/ByUpdationDate';
import BySource from 'components/Filters/BySource';
import ByCategory from 'components/Filters/ByCategory';


class FilterPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filters: {},
            errors: {},
        };
    }

    @autobind
    onChangeFilter(changes) {
        this.setState({ filters: { ...this.state.filters, ...changes } });
    }

    @autobind
    filtrate(isReset = false) {
        if(isReset) {
            this.setState({ filters: {} });
            this.props.onFilter({});
            return;
        }
        const filters = { ...this.state.filters };
        if(filters.created_at) {
            filters.created_at = {
                from: filters.created_at[0].toISOString(),
                to: filters.created_at[1].toISOString(),
            };
        }
        if(filters.updated_at) {
            filters.updated_at = {
                from: filters.updated_at[0].toISOString(),
                to: filters.updated_at[1].toISOString(),
            };
        }
        this.props.onFilter(filters);
    }

    render() {
        const { filters, errors } = this.state;
        const { allowedFilters } = this.props;
        return (
            <Row
                type="flex"
                justify="space-around"
                align="top"
                style={{
                    width: '100%',
                    marginBottom: '15px',
                }}
            >
                <Col span={16}>
                    {allowedFilters.includes('name') ? (
                        <ByName
                            value={filters.name}
                            onChange={e => this.onChangeFilter({ name: e.target.value })}
                        />
                    ) : null}
                    {allowedFilters.includes('created_at') ? (
                        <ByCreationDate
                            value={filters.created_at}
                            onChange={dates => this.onChangeFilter({ created_at: dates })}
                        />
                    ) : null}
                    {allowedFilters.includes('updated_at') ? (
                        <ByUpdationDate
                            value={filters.updated_at}
                            onChange={dates => this.onChangeFilter({ updated_at: dates })}
                        />
                    ) : null}
                    {allowedFilters.includes('source') ? (
                        <BySource
                            value={filters.source}
                            onChange={values => this.onChangeFilter({ source: values })}
                        />
                    ) : null}
                    {allowedFilters.includes('categories') ? (
                        <ByCategory
                            value={filters.categories}
                            onChange={values => this.onChangeFilter({ categories: values })}
                        />
                    ) : null}
                </Col>
                <Col span={8} />
                <Row
                    type="flex"
                    justify="space-around"
                    align="top"
                >
                    <Button
                        type="primary"
                        ghost
                        onClick={() => this.filtrate(false)}
                    >
                        Filter
                    </Button>
                    <Button
                        type="primary"
                        ghost
                        onClick={() => this.filtrate(true)}
                    >
                        Reset
                    </Button>
                </Row>
            </Row>
        );
    }
}

export default FilterPanel;
