import React, { Fragment } from 'react';

import autobind from 'autobind-decorator';
import { isEmpty, zip } from 'lodash';
import Papa from 'papaparse';

import { withRouter } from 'react-router-dom';

import {
    Button,
    Checkbox,
    Col,
    Divider,
    Form,
    Icon,
    Input,
    Modal,
    Row,
    Upload,
    Select,
    Spin,
} from 'antd';

import { SafeFetch, getErrorInfo } from 'lib/utils';


const Dragger = Upload.Dragger;
const Option = Select.Option;
const safeFetch = new SafeFetch();


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

const CategorySelect = ({
    disabled = false,
    categories,
    value,
    onChange,
}) => {
    return (
        <Select
            value={value}
            onChange={onChange}
            disabled={disabled}
        >
            {categories.map(category => (
                <Option
                    value={category._id}
                    key={category._id}
                >
                    {category.name}
                </Option>
            ))}
        </Select>
    );
};


async function parseFile(file) {
    return Promise((resolve, reject) => {
        Papa.parse(file, {
            complete: resolve,
            error: reject,
        });
    });
}


const FileRow = ({
    file,
    disableCategory,
    categories,
    selectedCategory,
    changeFileCategory,
    categoryError,
    nameError,
    onRemove,
    signalName,
    changeFileName,
}) => {
    return (
        <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '10px' }}>
            <Col span={8}>
                <Form.Item
                    label="Name"
                    {...formItemLayout}
                    {...getErrorInfo(nameError)}
                >
                    <Input
                        value={signalName}
                        onChange={changeFileName}
                    />
                </Form.Item>
            </Col>
            <Col span={12}>
                <Form.Item
                    label="Category"
                    {...formItemLayout}
                    {...getErrorInfo(categoryError)}
                >
                    <CategorySelect
                        disabled={disableCategory}
                        categories={categories}
                        value={selectedCategory}
                        onChange={changeFileCategory}
                    />
                </Form.Item>
            </Col>
            <Col span={2} style={{ marginBottom: '24px' }}>
                <Icon
                    type="close"
                    onClick={() => onRemove(file.uid)}
                    style={{ cursor: 'pointer' }}
                />
            </Col>
        </Row>
    );
};


class ImportPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filesList: [],
            isCreateNewCategory: true,
            isCategoryForAll: false,
            categories: [],
            categoryForAllId: null,
            filesCategories: {},
            filesNames: {},
            errors: {
                namesErrors: {},
                categoriesErrors: {},
            },
            categoryName: '',
            isLoading: false,
        };
    }

    componentDidMount() {
        this.fetchCategories();
    }

    @autobind
    changeCategoryName({ target }) {
        this.setState(() => ({ categoryName: target.value }));
    }

    @autobind
    toggleCreateNewCategory(e) {
        this.setState(() => ({ isCreateNewCategory: e.target.checked, isCategoryForAll: false }));
    }

    @autobind
    toggleCategoryForAll(e) {
        this.setState(() => ({ isCategoryForAll: e.target.checked, isCreateNewCategory: false }));
    }

    @autobind
    updateFilesList(filesList) {
        this.setState((state) => {
            const existingNames = state.filesList.map(f => f.name);
            const newNames = {};
            filesList.forEach((f) => {
                newNames[f.uid] = f.name;
            });
            return {
                filesList: [
                    ...state.filesList,
                    ...filesList.filter(f => !existingNames.includes(f.name)),
                ],
                filesNames: {
                    ...newNames,
                    ...state.filesNames,
                },
            };
        });
    }

    @autobind
    changeFileCategory(fileUid, categoryId) {
        this.setState({
            filesCategories: {
                ...this.state.filesCategories,
                [fileUid]: categoryId,
            },
        });
    }

    @autobind
    changeFileName(fileUid, name) {
        this.setState({
            filesNames: {
                ...this.state.filesNames,
                [fileUid]: name,
            },
        });
    }


    @autobind
    removeFile(fileUid) {
        this.setState({
            filesList: this.state.filesList.filter(f => f.uid !== fileUid),
        });
    }

    @autobind
    changeCategoryForAllId(categoryId) {
        this.setState({ categoryForAllId: categoryId });
    }

    @autobind
    async onSave() {
        const errors = {};
        const {
            isCategoryForAll,
            isCreateNewCategory,
            categoryForAllId,
            filesCategories,
            filesList,
            categoryName,
            filesNames,
        } = this.state;
        if(isEmpty(filesList)) {
            errors.filesList = 'Select at least one file to upload';
        }
        if(isCategoryForAll && !categoryForAllId) {
            errors.categoryForAllId = 'This field is required';
        }
        if(isCreateNewCategory && !categoryName) {
            errors.categoryName = 'This field is required';
        }
        const categoriesErrors = {};
        if(!isCreateNewCategory && !isCategoryForAll) {
            filesList.forEach((file) => {
                if(!filesCategories[file.uid]) categoriesErrors[file.uid] = 'This field id required';
            });
        }
        const namesErrors = {};
        filesList.forEach((file) => {
            if(!filesNames[file.uid]) namesErrors[file.uid] = 'This field id required';
        });
        let isValid = true;
        if(!isEmpty(errors) || !isEmpty(categoriesErrors) || !isEmpty(namesErrors)) isValid = false;
        errors.namesErrors = namesErrors;
        errors.categoriesErrors = categoriesErrors;
        this.setState({ errors });
        if(isValid) return;
        this.setState({ isLoading: false });
        const parsers = filesList.map(parseFile);
        const results = await Promise.all(parsers);
        const parsedData = results.map(r => r.data);
        const postData = {
            signals: zip(parsedData, filesList).map(([data, file]) => {
                let categoryId = null;
                if(isCategoryForAll) {
                    categoryId = categoryForAllId;
                } else if(!isCreateNewCategory) {
                    categoryId = filesCategories[file.uid];
                }
                return {
                    category_id: categoryId,
                    data,
                    file_name: file.name,
                    name: filesNames[file.uid],
                };
            }),
            category_name: isCreateNewCategory ? categoryName : null,
        };
        const answer = await safeFetch.put('/api/v1/signals/import', postData);
        this.setState({ isLoading: false });
        if(!SafeFetch.isError(answer)) {
            this.props.onClose();
        }
    }

    async fetchCategories() {
        const categories = await safeFetch.post('/api/v1/categories');
        if(SafeFetch.isError(categories)) return;
        this.setState(() => ({ categories }));
    }

    render() {
        const {
            onClose,
            visible,
        } = this.props;
        const {
            isCreateNewCategory,
            isCategoryForAll,
            categories,
            categoryForAllId,
            filesCategories,
            errors,
            filesList,
            filesNames,
        } = this.state;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                bodyStyle={{ padding: '20px 25px' }}
                z-index="2000"
                closable={false}
                title={
                    <Spin spinning={isLoading}>
                        <Row type="flex" justify="space-between">
                            <Col span={12}>
                                Import Signals
                            </Col>
                            <Col span={12}>
                                <Row type="flex" justify="end">
                                    <Button.Group>
                                        <Button
                                            type="primary"
                                            onClick={this.onSave}
                                        >
                                            Save
                                        </Button>
                                        <Button
                                            type="danger"
                                            ghost
                                            onClick={onClose}
                                        >
                                            Close
                                        </Button>
                                    </Button.Group>
                                </Row>
                            </Col>
                        </Row>
                    </Spin>
                }
                visible={visible}
                footer={null}
            >
                <Spin spinning={isLoading}>
                    <Row type="flex" justify="space-around" align="middle">
                        <Col span={24} style={{ height: '150px' }}>
                            <Dragger
                                name="file"
                                multiple
                                filesList={filesList}
                                beforeUpload={(file, fileList) => {
                                    this.updateFilesList(fileList);
                                    return false;
                                }}
                                showUploadList={false}
                            >
                                <p className="ant-upload-drag-icon">
                                    <Icon type="inbox" />
                                </p>
                                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                <p className="ant-upload-hint">Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files</p>
                            </Dragger>
                        </Col>
                    </Row>
                    {errors.filesList ? (
                        <Row
                            type="flex"
                            justify="space-around"
                            align="middle"
                            style={{ color: 'red', fontSize: '18px' }}
                        >
                            {errors.filesList}
                        </Row>
                    ) : null}
                    <Row
                        type="flex"
                        justify="space-around"
                        align="middle"
                        style={{ marginTop: '15px' }}
                    >
                        <Col span={10}>
                            <Form.Item
                                {...formItemLayout}
                            >
                                <Checkbox
                                    checked={isCategoryForAll}
                                    onChange={this.toggleCategoryForAll}
                                >
                                    Import signal to one category
                                </Checkbox>
                            </Form.Item>
                            <Form.Item
                                {...formItemLayout}
                            >
                                <Checkbox
                                    checked={isCreateNewCategory}
                                    onChange={this.toggleCreateNewCategory}
                                >
                                    Create New Category
                                </Checkbox>
                            </Form.Item>
                        </Col>
                        <Col span={14}>
                            <Form.Item
                                label="Category name"
                                {...formItemLayout}
                                {...getErrorInfo(errors.categoryName)}
                            >
                                <Input
                                    disabled={!isCreateNewCategory}
                                    value={this.state.categoryName}
                                    onChange={this.changeCategoryName}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    {filesList.length ? <Divider /> : null}
                    <Row
                        type="flex"
                        justify="space-around"
                        align="middle"
                    >
                        <Col span={24}>
                            {filesList.map((file) => {
                                return (
                                    <FileRow
                                        disableCategory={isCategoryForAll || isCreateNewCategory}
                                        key={file.uid}
                                        file={file}
                                        categories={categories}
                                        selectedCategory={filesCategories[file.uid]}
                                        signalName={filesNames[file.uid]}
                                        changeFileName={e => this.changeFileName(file.uid, e.target.value)}
                                        nameError={errors.namesErrors[file.uid]}
                                        changeFileCategory={
                                            categoryId => this.changeFileCategory(file.uid, categoryId)
                                        }
                                        categoryError={errors.categoriesErrors[file.uid]}
                                        onRemove={this.removeFile}
                                    />
                                );
                            })}
                        </Col>
                    </Row>
                </Spin>
            </Modal>
        );
    }
}


export default ImportPopup;
