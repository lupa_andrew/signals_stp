import React from 'react';
import PropTypes from 'prop-types';

import {
    Modal,
    Row,
    Col,
    Form,
    Checkbox,
    Select,
    Button,
    Spin,
} from 'antd';
import autobind from 'autobind-decorator';
import { isEmpty } from 'lodash';

import { SafeFetch, getErrorInfo } from 'lib/utils';

import CategorySelect from 'components/CategorySelect';


const formItemLayout = {
    label: {
        span: 8,
    },
    wrapper: {
        span: 16,
    },
};


const safeFetch = SafeFetch;


export default class ApplyScriptPopup extends React.Component {

    static propTypes = {
        onClose: PropTypes.func,
        visible: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            categories: [],
            signals: [],
            scripts: [],
            signalId: null,
            categoryId: null,
            scriptId: null,
            errors: {},
            overrideSignal: false,
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        this.setState({ isLoading: true });
        const [categories, signals, scripts] = await Promise.all([
            safeFetch.get('/api/v1/categories'),
            safeFetch.get('/api/v1/signals'),
            safeFetch.get('/api/v1/scripts'),
        ]);
        if(
            SafeFetch.isError(categories)
            || SafeFetch.isError(signals)
            || SafeFetch.isError(scripts)
        ) return;
        this.setState({
            categories,
            signals,
            scripts,
            isLoading: false,
        });
    }

    @autobind
    onApply() {
        const errors = {};
        const { signalId, categoryId, scriptId, overrideSignal } = this.state;
        if(!categoryId) errors.categoryId = 'This field is required';
        if(!signalId) errors.signalId = 'This field is required';
        this.setState({ isLoading: true });
        const res = safeFetch(`/api/v1/signal/${signalId}/apply`, {
            script_id: scriptId,
            override_signal: overrideSignal,
        });
        if(SafeFetch.isError(res)) return;
        window.location.load();
    }

    render() {
        const { onClose, visible } = this.props;
        const {
            errors,
            signalId,
            categoryId,
            scriptId,
            scripts,
            categories,
            signals,
            overrideSignal,
            isLoading,
        } = this.state;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                z-index="2000"
                closable={false}
                title={
                    <Spin spinning={isLoading}>
                        <Row type="flex" justify="space-between">
                            <Col span={12}>
                                Applying script to signal
                            </Col>
                            <Col span={12}>
                                <Row type="flex" justify="end">
                                    <Button.Group>
                                        <Button
                                            type="primary"
                                            onClick={this.onApply}
                                        >
                                            Apply
                                        </Button>
                                        <Button
                                            type="danger"
                                            ghost
                                            onClick={onClose}
                                        >
                                            Close
                                        </Button>
                                    </Button.Group>
                                </Row>
                            </Col>
                        </Row>
                    </Spin>
                }
                visible={visible}
                footer={null}
            >
                <Spin spinning={isLoading}>
                    <Row type="flex" justify="space-around" align="middle" style={{ minHeight: '200px' }}>
                        <Col span={18}>
                            <Form.Item
                                label="Data set"
                                {...formItemLayout}
                                {...getErrorInfo(errors.categoryId)}
                            >
                                <CategorySelect
                                    value={categoryId}
                                    categories={categories}
                                    onChange={value => this.setState({ categoryId: value })}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Signal"
                                {...formItemLayout}
                                {...getErrorInfo(errors.signalId)}
                            >
                                <Select
                                    disabled={!categoryId}
                                    value={signalId}
                                    onChange={s => this.setState({ signalId: s })}
                                    style={{ width: '100%' }}
                                >
                                    {signals.filter(s => s.category_id === categoryId).map(s => (
                                        <Select.Option
                                            value={s._id}
                                            key={s._id}
                                        >
                                            {s.name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            <Form.Item
                                label="Signal type"
                                {...formItemLayout}
                                {...getErrorInfo(errors.type)}
                            >
                                <Select
                                    value={scriptId}
                                    onChange={s => this.setState({ scriptId: s })}
                                    style={{ width: '100%' }}
                                >
                                    {scripts.map(s => (
                                        <Select.Option
                                            value={s._id}
                                            key={s._id}
                                        >
                                            {s.name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            <Form.Item
                                {...formItemLayout}
                            >
                                <Checkbox
                                    checked={overrideSignal}
                                    onChange={e => this.setState({ overrideSignal: e.target.checked })}
                                >
                                    Override signal by applying
                                </Checkbox>
                            </Form.Item>
                        </Col>
                    </Row>
                </Spin>
            </Modal>
        );
    }
}