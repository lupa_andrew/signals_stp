import React from 'react';

import {
    Form,
    Row,
    Col,
    Input,
    InputNumber,
    Icon,
    Spin,
} from 'antd';

import CategorySelect from 'components/CategorySelect';
import getErrorInfo from 'lib/utils';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const DbSignalRow = ({
    categories,
    selectedCategory,
    changeSignalCategory,
    categoryError,
    nameError,
    changeSignalName,
    signalName,
    onImportSignal,
    signal,
    additionalInfo,
    additionalInfoErrors,
    changeAdditionalInfo,
    isLoading = false,
}) => {
    return (
        <Spin spinning={isLoading}>
            <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '10px' }}>
                <Col span={8}>
                    <Form.Item
                        label="Name"
                        {...formItemLayout}
                        {...getErrorInfo(nameError)}
                    >
                        <Input
                            value={signalName}
                            onChange={changeSignalName}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item
                        label="Data set"
                        {...formItemLayout}
                        {...getErrorInfo(categoryError)}
                    >
                        <CategorySelect
                            categories={categories}
                            value={selectedCategory}
                            onChange={changeSignalCategory}
                        />
                    </Form.Item>
                </Col>
                <Col span={2} style={{ marginBottom: '24px' }}>
                    <Icon
                        type="download"
                        onClick={() => onImportSignal(signal._id)}
                        style={{ cursor: 'pointer' }}
                    />
                </Col>
            </Row>
            <Row type="flex" justify="space-around" align="top" style={{ marginTop: '10px' }}>
                <Col span={7}>
                    <Form.Item
                        label="Sampling frequency"
                        {...formItemLayout}
                        {...getErrorInfo(additionalInfoErrors.step)}
                    >
                        <InputNumber
                            value={additionalInfo.step}
                            min={1}
                            onChange={value => changeAdditionalInfo({ step: value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={7}>
                    <Form.Item
                        label="Signal type"
                        {...formItemLayout}
                        {...getErrorInfo(additionalInfoErrors.type)}
                    >
                        <SignalTypeSelect
                            value={additionalInfo.type}
                            onChange={value => changeAdditionalInfo({ type: value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={10} style={{ marginBottom: '24px' }}>
                    <Form.Item
                        label="Comment"
                        {...formItemLayout}
                        {...getErrorInfo(additionalInfoErrors.comment)}
                    >
                        <Input.TextArea
                            value={additionalInfo.comment}
                            onChange={e => changeAdditionalInfo({ comment: e.target.value })}
                        />
                    </Form.Item>
                </Col>
            </Row>
        </Spin>
    );
};


export default DbSignalRow;
