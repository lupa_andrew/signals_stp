import React, { Fragment } from 'react';

import autobind from 'autobind-decorator';
import { isEmpty, zip, isNumber } from 'lodash';
import Papa from 'papaparse';


import {
    Button,
    Checkbox,
    Col,
    Divider,
    Form,
    Icon,
    Input,
    InputNumber,
    Modal,
    Row,
    Upload,
    Select,
    Spin,
    Tabs,
} from 'antd';

import { SafeFetch, getErrorInfo } from 'lib/utils';

import notify from 'components/notifications';
import CategorySelect from 'components/CategorySelect';

import FileRow from './FileRow';
import DbSignalRow from './DbSignalRow';


const Dragger = Upload.Dragger;
const Option = Select.Option;
const safeFetch = SafeFetch;
const TabPane = Tabs.TabPane;


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const FILES_IMPORT_TAB = 'files_import';
const DB_IMPORT_TAB = 'db_import';



async function parseFile(file) {
    return new Promise((resolve, reject) => {
        Papa.parse(file, {
            complete: resolve,
            error: reject,
        });
    });
}


class ImportPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filesList: [],
            isCreateNewCategory: true,
            isCategoryForAll: false,
            categories: [],
            categoryForAllId: null,
            filesCategories: {},
            filesNames: {},
            additionalInfos: {},
            additionalInfosErrors: {},
            errors: {
                namesErrors: {},
                categoriesErrors: {},
            },
            categoryName: '',
            isLoading: false,
            currentTab: FILES_IMPORT_TAB,
            dbConnectionConfig: {
                // host: '',
                // port: '',
                // name: '',
                // user: '',
                // password: '',
                port: '17970',
                db: 'signals',
                host: 'ds217970.mlab.com',
                user: 'application',
                password: 'd41d8cd98f00b204e9800ecf8427e',
                collection: 'signals',
            },
            dbStatusConnected: false,
            dbConfigErrors: {},
            dbImportConfig: {},
            dbImportConfigErrors: {},
            dbSignals: [],
            dbSignalsCategories: {},
            dbSignalsNames: {},
            dbSignalsNamesErrors: {},
            dbSignalsCategoriesErrors: {},
            dbImportingSignalsLoadingStatuses: {},
            dbAdditionalInfos: {},
            dbAdditionalInfosErrors: {},
        };
    }

    componentDidMount() {
        this.fetchCategories();
    }

    @autobind
    changeCategoryName({ target }) {
        this.setState(() => ({ categoryName: target.value }));
    }

    @autobind
    toggleCreateNewCategory(e) {
        this.setState(() => ({ isCreateNewCategory: e.target.checked, isCategoryForAll: false }));
    }

    @autobind
    toggleCategoryForAll(e) {
        this.setState(() => ({ isCategoryForAll: e.target.checked, isCreateNewCategory: false }));
    }

    @autobind
    updateFilesList(filesList) {
        this.setState((state) => {
            const existingNames = state.filesList.map(f => f.name);
            const newNames = {};
            filesList.forEach((f) => {
                newNames[f.uid] = f.name;
            });
            return {
                filesList: [
                    ...state.filesList,
                    ...filesList.filter(f => !existingNames.includes(f.name)),
                ],
                filesNames: {
                    ...newNames,
                    ...state.filesNames,
                },
            };
        });
    }

    @autobind
    changeFileCategory(fileUid, categoryId) {
        this.setState({
            filesCategories: {
                ...this.state.filesCategories,
                [fileUid]: categoryId,
            },
        });
    }

    changeAdditionalInfo(fileUid, changes) {
        this.setState({
            additionalInfos: {
                ...this.state.additionalInfos,
                [fileUid]: {
                    ...this.state.additionalInfos[fileUid],
                    ...changes,
                },
            },
        });
    }

    changeDbAdditionalInfo(signalId, changes) {
        this.setState({
            dbAdditionalInfos: {
                ...this.state.dbAdditionalInfos,
                [signalId]: {
                    ...this.state.dbAdditionalInfos[signalId],
                    ...changes,
                },
            },
        });
    }

    @autobind
    changeSignalCategory(signalId, categoryId) {
        this.setState({
            dbSignalsCategories: {
                ...this.state.dbSignalsCategories,
                [signalId]: categoryId,
            },
        });
    }

    @autobind
    changeFileName(fileUid, name) {
        this.setState({
            filesNames: {
                ...this.state.filesNames,
                [fileUid]: name,
            },
        });
    }

    @autobind
    changeSignalName(signalId, name) {
        this.setState({
            dbSignalsNames: {
                ...this.state.dbSignalsNames,
                [signalId]: name,
            },
        });
    }


    @autobind
    removeFile(fileUid) {
        this.setState({
            filesList: this.state.filesList.filter(f => f.uid !== fileUid),
        });
    }

    @autobind
    changeCategoryForAllId(categoryId) {
        this.setState({ categoryForAllId: categoryId });
    }

    @autobind
    async onSave() {
        const errors = {};
        const {
            isCategoryForAll,
            isCreateNewCategory,
            categoryForAllId,
            filesCategories,
            filesList,
            categoryName,
            filesNames,
            additionalInfos,
        } = this.state;
        if(isEmpty(filesList)) {
            errors.filesList = 'Select at least one file to upload';
        }
        if(isCategoryForAll && !categoryForAllId) {
            errors.categoryForAllId = 'This field is required';
        }
        if(isCreateNewCategory && !categoryName) {
            errors.categoryName = 'This field is required';
        }
        const categoriesErrors = [];
        if(!isCreateNewCategory && !isCategoryForAll) {
            filesList.forEach((file) => {
                if(!filesCategories[file.uid]) categoriesErrors[file.uid] = 'This field id required';
            });
        }
        const namesErrors = [];
        filesList.forEach((file) => {
            if(!filesNames[file.uid]) namesErrors[file.uid] = 'This field id required';
        });
        let isValid = true;
        if(isEmpty(errors) || !isEmpty(categoriesErrors) || !isEmpty(namesErrors)) isValid = false;
        errors.namesErrors = namesErrors;
        errors.categoriesErrors = categoriesErrors;
        this.setState({ errors });
        if(isValid) return;
        this.setState({ isLoading: true });
        const parsers = filesList.map(parseFile);
        const results = await Promise.all(parsers);
        const parsedData = results.map(r => r.data);
        const postData = {
            signals: zip(parsedData, filesList).map(([data, file]) => {
                let categoryId = null;
                if(isCategoryForAll) {
                    categoryId = categoryForAllId;
                } else if(!isCreateNewCategory) {
                    categoryId = filesCategories[file.uid];
                }
                return {
                    category_id: categoryId,
                    data,
                    file_name: file.name,
                    name: filesNames[file.uid],
                    ...additionalInfos[file.uid],
                };
            }),
            category_name: isCreateNewCategory ? categoryName : null,
        };
        const answer = safeFetch.post('/api/v1/signal/import', postData);
        this.setState({ isLoading: false });
        if(!SafeFetch.isError(answer)) {
            this.props.onClose();
        }
    }

    @autobind
    async onImportSignal(signalId) {
        const signalName = this.state.dbSignalsNames[signalId];
        const signalNameError = !isEmpty(signalName) ? null : 'This Field is required';
        const signalCategory = this.state.dbSignalsCategories[signalId];
        const signalCategoryError = !isEmpty(signalCategory) ? null : 'This Field is required';
        this.setState({
            dbSignalsCategoriesErrors: {
                ...this.state.dbSignalsCategoriesErrors,
                [signalId]: signalCategoryError,
            },
            dbSignalsNamesErrors: {
                ...this.state.dbSignalsNamesErrors,
                [signalId]: signalNameError,
            },
            dbAdditionalInfosErrors: {
                ...this.state.dbAdditionalInfosErrors,
                [signalId]: dbAdditionalInfoError,
            },
        });
        if(signalNameError || signalCategoryError || !isEmpty(dbAdditionalInfoError)) return;
        const { dbConnectionConfig } = this.state;
        this.toggleSignalImportingState(signalId, true);
        const answer = safeFetch.put('/api/v1/signals/import/import_from_db', {
            ...dbConnectionConfig,
            name: signalName,
            category_id: signalCategory,
            ...dbAdditionalInfo,
            _id: signalId,
        });
        this.toggleSignalImportingState(signalId, false);
        if(SafeFetch.isError(answer)) return;
        if(answer.status === 'FAILED') {
            notify.error('Error!', `Importing Signal "${signalName}" failed!`);
        }
        if(answer.status === 'OK') {
            notify.success('Congratulations!', `Signal "${signalName}" was successfully imported!`);
        }
    }

    @autobind
    toggleSignalImportingState(signalId, status) {
        this.setState((state) => {
            return {
                dbImportingSignalsLoadingStatuses: {
                    ...state.dbImportingSignalsLoadingStatuses,
                    [signalId]: status,
                },
            };
        });
    }

    @autobind
    onChangeTab(currentTab) {
        this.setState({ currentTab });
    }

    @autobind
    changeDbConfig(changes) {
        this.setState({
            dbConnectionConfig: { ...this.state.dbConnectionConfig, ...changes },
            dbStatusConnected: false,
            dbImportConfig: {},
            dbImportConfigErrors: {},
            dbSignals: [],
            dbSignalsCategories: {},
            dbSignalsNames: {},
            dbSignalsCategoriesErrors: {},
            dbSignalsNamesErrors: {},
        });
    }

    @autobind
    changeDbImportConfig(changes) {
        this.setState({
            dbImportConfig: { ...this.state.dbImportConfig, ...changes },
        });
    }

    @autobind
    async loadFromDb() {
        const { dbImportConfig } = this.state;
        const dbImportConfigErrors = {};
        if(!dbImportConfig.offset && dbImportConfig.offset !== 0) {
            dbImportConfigErrors.offset = 'This field is required';
        } else if(!isNumber(dbImportConfig.offset)) {
            dbImportConfigErrors.offset = 'Value must be number';
        }
        if(!dbImportConfig.limit) {
            dbImportConfigErrors.limit = 'This field is required';
        } else if(!isNumber(dbImportConfig.limit)) {
            dbImportConfigErrors.limit = 'Value must be number';
        }
        this.setState({ dbImportConfigErrors });
        if(!isEmpty(dbImportConfigErrors)) return;
        const { dbConnectionConfig } = this.state;
        const answer = await safeFetch.post('/api/v1/signals/import/load_from_db', {
            ...dbConnectionConfig,
            ...dbImportConfig,
        });
        if(SafeFetch.isError(answer)) return;
        const dbSignals = answer.signals;
        const dbSignalsNames = {};
        dbSignals.forEach((s) => {
            dbSignalsNames[s._id] = s.name;
        });
        this.setState({
            dbSignals,
            dbSignalsCategories: {},
            dbSignalsNames,
        });
    }

    @autobind
    async checkDbConnection() {
        const { dbConnectionConfig } = this.state;
        const dbConfigErrors = {};
        if(dbConnectionConfig.host) dbConfigErrors.host = 'This field is required';
        if(dbConnectionConfig.port) dbConfigErrors.port = 'This field is required';
        if(dbConnectionConfig.db) dbConfigErrors.db = 'This field is required';
        if(dbConnectionConfig.user) dbConfigErrors.user = 'This field is required';
        if(dbConnectionConfig.password) dbConfigErrors.password = 'This field is required';
        if(dbConnectionConfig.collection) dbConfigErrors.password = 'This field is required';
        this.setState({ dbConfigErrors });
        if(!isEmpty(dbConfigErrors)) return;
        const connectionInfo = safeFetch.post('/api/v1/signal/import/check_db_connection', dbConnectionConfig);
        if(SafeFetch.isError(connectionInfo)) return;
        this.setState({ dbStatusConnected: connectionInfo.status === 'OK' });
    }

    async fetchCategories() {
        const categories = await safeFetch.get('/api/v1/categories');
        if(SafeFetch.isError(categories)) return;
        this.setState(() => ({ categories }));
    }

    render() {
        const {
            onClose,
            visible,
        } = this.props;
        const {
            isCreateNewCategory,
            isCategoryForAll,
            categories,
            categoryForAllId,
            filesCategories,
            additionalInfos,
            additionalInfosErrors,
            dbAdditionalInfos,
            dbAdditionalInfosErrors,
            errors,
            filesList,
            filesNames,
            isLoading,
            currentTab,
            dbConfigErrors,
            dbConnectionConfig,
            dbStatusConnected,
            dbImportConfig,
            dbImportConfigErrors,
            dbSignals,
            dbSignalsCategories,
            dbSignalsNames,
            dbSignalsNamesErrors,
            dbSignalsCategoriesErrors,
            dbImportingSignalsLoadingStatuses,
        } = this.state;
        return (
            <Modal
                width={1200}
                style={{ top: '20px', height: '100%' }}
                bodyStyle={{ padding: '20px 25px' }}
                z-index="2000"
                closable={false}
                title={
                    <Spin spinning={isLoading}>
                        <Row type="flex" justify="space-between">
                            <Col span={12}>
                                Import Signals
                            </Col>
                            <Col span={12}>
                                <Row type="flex" justify="end">
                                    <Button.Group>
                                        {currentTab === FILES_IMPORT_TAB ? (
                                            <Button
                                                type="primary"
                                                onClick={this.onSave}
                                            >
                                                Save
                                            </Button>
                                        ) : null}
                                        <Button
                                            type="danger"
                                            ghost
                                            onClick={onClose}
                                        >
                                            Close
                                        </Button>
                                    </Button.Group>
                                </Row>
                            </Col>
                        </Row>
                    </Spin>
                }
                visible={visible}
                footer={null}
            >
                <Spin spinning={isLoading}>
                    <Tabs activeKey={currentTab} onChange={this.onChangeTab}>
                        <TabPane tab="Import from file" key={FILES_IMPORT_TAB} style={{ height: 'auto' }}>
                            <Row type="flex" justify="space-around" align="middle">
                                <Col span={24} style={{ height: '150px' }}>
                                    <Dragger
                                        name="file"
                                        multiple
                                        filesList={filesList}
                                        beforeUpload={(file, fileList) => {
                                            this.updateFilesList(fileList);
                                            return false;
                                        }}
                                        showUploadList={false}
                                    >
                                        <p className="ant-upload-drag-icon">
                                            <Icon type="inbox" />
                                        </p>
                                        <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                        <p className="ant-upload-hint">Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files</p>
                                    </Dragger>
                                </Col>
                            </Row>
                            {errors.filesList ? (
                                <Row
                                    type="flex"
                                    justify="space-around"
                                    align="middle"
                                    style={{ color: 'red', fontSize: '18px' }}
                                >
                                    {errors.filesList}
                                </Row>
                            ) : null}
                            <Row
                                type="flex"
                                justify="space-around"
                                align="middle"
                                style={{ marginTop: '15px' }}
                            >
                                <Col span={10}>
                                    <Form.Item
                                        {...formItemLayout}
                                    >
                                        <Checkbox
                                            checked={isCategoryForAll}
                                            onChange={this.toggleCategoryForAll}
                                        >
                                            Import signal to one Data set
                                        </Checkbox>
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemLayout}
                                    >
                                        <Checkbox
                                            checked={isCreateNewCategory}
                                            onChange={this.toggleCreateNewCategory}
                                        >
                                            Create New Data set
                                        </Checkbox>
                                    </Form.Item>
                                </Col>
                                <Col span={14}>
                                    <Form.Item
                                        label="Data set"
                                        {...formItemLayout}
                                        {...getErrorInfo(errors.categoryForAllId)}
                                    >
                                        <CategorySelect
                                            disabled={!isCategoryForAll}
                                            categories={categories}
                                            value={categoryForAllId}
                                            onChange={this.changeCategoryForAllId}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        label="Data set name"
                                        {...formItemLayout}
                                        {...getErrorInfo(errors.categoryName)}
                                    >
                                        <Input
                                            disabled={!isCreateNewCategory}
                                            value={this.state.categoryName}
                                            onChange={this.changeCategoryName}
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                            {filesList.length ? <Divider /> : null}
                            <Row
                                type="flex"
                                justify="space-around"
                                align="middle"
                            >
                                <Col span={24}>
                                    {filesList.map((file) => {
                                        return (
                                            <React.Fragment>
                                                <FileRow
                                                    disableCategory={isCategoryForAll || isCreateNewCategory}
                                                    key={file.uid}
                                                    file={file}
                                                    categories={categories}
                                                    selectedCategory={filesCategories[file.uid]}
                                                    signalName={filesNames[file.uid]}
                                                    changeFileName={e => this.changeFileName(file.uid, e.target.value)}
                                                    nameError={errors.namesErrors[file.uid]}
                                                    changeFileCategory={
                                                        categoryId => this.changeFileCategory(file.uid, categoryId)
                                                    }
                                                    categoryError={errors.categoriesErrors[file.uid]}
                                                    additionalInfo={additionalInfos[file.uid] || {}}
                                                    additionalInfoErrors={additionalInfosErrors[file.uid] || {}}
                                                    changeAdditionalInfo={
                                                        changes => this.changeAdditionalInfo(file.uid, changes)
                                                    }
                                                    onRemove={this.removeFile}
                                                />
                                                <Divider />
                                            </React.Fragment>
                                        );
                                    })}
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tab="Import from another DB" key={DB_IMPORT_TAB} style={{ height: 'auto' }}>
                            <Row
                                type="flex"
                                justify="space-around"
                                align="middle"
                                style={{ marginTop: '15px' }}
                            >
                                <Col span={12}>
                                    <Form.Item
                                        {...formItemLayout}
                                        {...getErrorInfo(dbConfigErrors.host)}
                                        label="DB Host"
                                    >
                                        <Input
                                            value={dbConnectionConfig.host}
                                            onChange={e => this.changeDbConfig({ host: e.target.value })}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemLayout}
                                        {...getErrorInfo(dbConfigErrors.port)}
                                        label="DB Port"
                                    >
                                        <Input
                                            value={dbConnectionConfig.port}
                                            onChange={e => this.changeDbConfig({ port: e.target.value })}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemLayout}
                                        {...getErrorInfo(dbConfigErrors.db)}
                                        label="DB Name"
                                    >
                                        <Input
                                            value={dbConnectionConfig.db}
                                            onChange={e => this.changeDbConfig({ db: e.target.value })}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        {...formItemLayout}
                                        {...getErrorInfo(dbConfigErrors.user)}
                                        label="DB User"
                                    >
                                        <Input
                                            value={dbConnectionConfig.user}
                                            onChange={e => this.changeDbConfig({ user: e.target.value })}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemLayout}
                                        {...getErrorInfo(dbConfigErrors.password)}
                                        label="DB Password"
                                    >
                                        <Input
                                            value={dbConnectionConfig.password}
                                            onChange={e => this.changeDbConfig({ password: e.target.value })}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemLayout}
                                        {...getErrorInfo(dbConfigErrors.collection)}
                                        label="Collection"
                                    >
                                        <Input
                                            value={dbConnectionConfig.collection}
                                            onChange={e => this.changeDbConfig({ collection: e.target.value })}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        {...{
                                            labelCol: {
                                                span: 12,
                                            },
                                            wrapperCol: {
                                                span: 12,
                                            },
                                        }}
                                        label={
                                            <span style={{ color: dbStatusConnected ? 'green' : 'red' }}>
                                                {dbStatusConnected ? 'Connected!' : 'Unknown connection!'}
                                            </span>
                                        }
                                    >
                                        <Button
                                            onClick={this.checkDbConnection}
                                        >
                                            Check connection
                                        </Button>
                                    </Form.Item>
                                </Col>
                            </Row>
                            {dbStatusConnected ? (<React.Fragment>
                                <Divider />
                                <Row type="flex" justify="space-around" align="middle">
                                    <Col span={12}>
                                        <Form.Item
                                            {...formItemLayout}
                                            {...getErrorInfo(dbImportConfigErrors.offset)}
                                            label="Offset"
                                        >
                                            <InputNumber
                                                min={0}
                                                value={dbImportConfig.offset}
                                                onChange={value => this.changeDbImportConfig({ offset: value })}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col span={12}>
                                        <Form.Item
                                            {...formItemLayout}
                                            {...getErrorInfo(dbImportConfigErrors.limit)}
                                            label="Limit"
                                        >
                                            <InputNumber
                                                min={1}
                                                value={dbImportConfig.limit}
                                                onChange={value => this.changeDbImportConfig({ limit: value })}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row type="flex" justify="space-around" align="middle">
                                    <Button
                                        onClick={this.loadFromDb}
                                    >
                                        Load signals
                                    </Button>
                                </Row>
                                {dbSignals.map((signal) => {
                                    return (
                                        <React.Fragment>
                                            <DbSignalRow
                                                key={signal._id}
                                                signal={signal}
                                                categories={categories}
                                                selectedCategory={dbSignalsCategories[signal._id]}
                                                signalName={dbSignalsNames[signal._id]}
                                                changeSignalName={e => this.changeSignalName(signal._id, e.target.value)}
                                                nameError={dbSignalsNamesErrors[signal._id]}
                                                changeSignalCategory={
                                                    categoryId => this.changeSignalCategory(signal._id, categoryId)
                                                }
                                                categoryError={dbSignalsCategoriesErrors[signal._id]}
                                                additionalInfo={dbAdditionalInfos[signal._id] || {}}
                                                additionalInfoErrors={dbAdditionalInfosErrors[signal._id] || {}}
                                                changeAdditionalInfo={
                                                    changes => this.changeDbAdditionalInfo(signal._id, changes)
                                                }
                                                onImportSignal={this.onImportSignal}
                                                isLoading={dbImportingSignalsLoadingStatuses[signal._id]}
                                            />
                                            <Divider />
                                        </React.Fragment>
                                    );
                                })}
                            </React.Fragment>) : null}
                        </TabPane>
                    </Tabs>
                </Spin>
            </Modal>
        );
    }
}


export default ImportPopup;
