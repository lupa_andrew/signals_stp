import React from 'react';

import {
    Form,
    Row,
    Col,
    Input,
    InputNumber,
    Icon,
} from 'antd';

import CategorySelect from 'components/CategorySelect';
import getErrorInfo from 'lib/utils';


const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};


const FileRow = ({
    file,
    disableCategory,
    categories,
    selectedCategory,
    changeFileCategory,
    categoryError,
    nameError,
    onRemove,
    signalName,
    changeFileName,
    additionalInfo,
    changeAdditionalInfo,
    additionalInfoErrors,
}) => {
    return (
        <React.Fragment>
            <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '10px' }}>
                <Col span={8}>
                    <Form.Item
                        label="Name"
                        {...formItemLayout}
                        {...getErrorInfo(nameError)}
                    >
                        <Input
                            value={signalName}
                            onChange={changeFileName}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item
                        label="Data set"
                        {...formItemLayout}
                        {...getErrorInfo(categoryError)}
                    >
                        <CategorySelect
                            disabled={disableCategory}
                            categories={categories}
                            value={selectedCategory}
                            onChange={changeFileCategory}
                        />
                    </Form.Item>
                </Col>
                <Col span={2} style={{ marginBottom: '24px' }}>
                    <Icon
                        type="close"
                        onClick={() => onRemove(file.uid)}
                        style={{ cursor: 'pointer' }}
                    />
                </Col>
            </Row>
            <Row type="flex" justify="space-around" align="top" style={{ marginTop: '10px' }}>
                <Col span={7}>
                    <Form.Item
                        label="Sampling frequency"
                        {...formItemLayout}
                        {...getErrorInfo(additionalInfoErrors.step)}
                    >
                        <InputNumber
                            value={additionalInfo.step}
                            min={1}
                            onChange={value => changeAdditionalInfo({ step: value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={7}>
                    <Form.Item
                        label="Signal type"
                        {...formItemLayout}
                        {...getErrorInfo(additionalInfoErrors.type)}
                    >
                        <SignalTypeSelect
                            value={additionalInfo.type}
                            onChange={value => changeAdditionalInfo({ type: value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={10} style={{ marginBottom: '24px' }}>
                    <Form.Item
                        label="Comment"
                        {...formItemLayout}
                        {...getErrorInfo(additionalInfoErrors.comment)}
                    >
                        <Input.TextArea
                            value={additionalInfo.comment}
                            onChange={e => changeAdditionalInfo({ comment: e.target.value })}
                        />
                    </Form.Item>
                </Col>
            </Row>
        </React.Fragment>
    );
};


export default FileRow;
