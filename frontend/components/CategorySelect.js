import React from 'react';

import { Select } from 'antd';


const Option = Select.Option;


const CategorySelect = ({
    disabled = false,
    categories,
    value,
    onChange,
}) => {
    return (
        <Select
            value={value}
            onChange={onChange}
            disabled={disabled}
            style={{ width: '100%' }}
        >
            {categories.map(category => (
                <Option
                    value={category._id}
                    key={category._id}
                >
                    {category.name}
                </Option>
            ))}
        </Select>
    );
};


export default CategorySelect;
