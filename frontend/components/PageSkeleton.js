import React from 'react';
import PropTypes from 'prop-types';

import { Router, Route, Switch, NavLink, withRouter, Link } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

import { Layout, Menu, Icon, Row } from 'antd';

const { Header, Sider, Content } = Layout;


const MENU_CONFIG = [
    {
        link: '/categories',
        title: 'Data sets',
        icon: 'user',
    },
    {
        link: '/categories/signals',
        title: 'Signals',
        icon: 'line-chart',
    },
    {
        link: '/scripts',
        title: 'Scripts',
        icon: 'code',
    },
];


class PageSkeleton extends React.Component {

    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.element),
            PropTypes.element,
        ]),
    };

    state = {
        collapsed: false,
    };

    history = createBrowserHistory();

    toggle() {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
        return (
            <Router history={this.history}>
                <Layout style={{ height: '100%' }}>
                    <Sider
                        collapsed={this.state.collapsed}
                        onCollapse={this.toggle}
                    >
                        <div className="logo" />
                        <Switch>
                            <Route
                                render={({ location }) => {
                                    const defaultSelectedKeys = MENU_CONFIG.reduce((acc, i) => {
                                        if(location.pathname.includes(i.link)) return [i.link];
                                        return acc;
                                    }, []);
                                    return (
                                        <Menu theme="dark" mode="inline" defaultSelectedKeys={defaultSelectedKeys}>
                                            {MENU_CONFIG.map(i => (
                                                <Menu.Item key={i.link}>
                                                    <NavLink exac to={i.link}>
                                                        <Icon type={i.icon} />
                                                        <span>{i.title}</span>
                                                    </NavLink>
                                                </Menu.Item>
                                            ))}
                                        </Menu>
                                    );
                                }}
                            />
                        </Switch>
                    </Sider>
                    <Switch>
                        {this.props.children}
                    </Switch>
                </Layout>
            </Router>
        );
    }
}

export default PageSkeleton;
