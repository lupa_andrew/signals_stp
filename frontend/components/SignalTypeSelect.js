import React from 'react';

import { Select } from 'antd';


const Option = Select.Option;

const VALUES = [
    'ECG',
    'HRV',
    'EEG',
    'GSR',
    'EMG',
    'PPG',
    'TEMP',
];

const SignalTypeSelect = ({
    disabled = false,
    value,
    onChange,
}) => {
    return (
        <Select
            value={value}
            onChange={onChange}
            disabled={disabled}
            style={{ width: '100%' }}
        >
            {VALUES.map(t => (
                <Option
                    value={t}
                    key={t}
                >
                    {t}
                </Option>
            ))}
        </Select>
    );
};


export default SignalTypeSelect;
