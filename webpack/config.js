const path = require('path');
const { version } = require('../package.json');
const { getEntries } = require('./helpers');


const paramToEntryMapper = {
    main: {
        'builds/main': './frontend/main.js',
    },
};


module.exports = {
    entry: getEntries(paramToEntryMapper),
    output: {
        filename: `[name]${version}.js`,
        path: path.resolve(__dirname, '../public/js/'),
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: [
                        'babel-plugin-transform-runtime',
                    ],
                },
            },

        ],
    },
    resolve: {
        modules: [
            path.resolve(__dirname, '../frontend'),
            'node_modules',
        ],
        extensions: ['.js', '.jsx'],
    },
};
