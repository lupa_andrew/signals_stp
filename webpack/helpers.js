const webpack = require('webpack');
const _ = require('lodash');

function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}

module.exports.getEntries = function (paramToEntryMapper) {
    const entryName = process.env.ENTRY;
    const entries = {};
    if(entryName) {
        const entry = paramToEntryMapper[entryName];
        if(!isEmptyObject(entry)) Object.assign(entries, entry);
    }
    if(isEmptyObject(entries)) {
        Object.keys(paramToEntryMapper).forEach((name) => {
            Object.assign(entries, paramToEntryMapper[name]);
        });
    }
    return entries;
};

module.exports.prodExtension = {
    devtool: 'cheap-module-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            sourceMap: false,
            compress: {
                warnings: false, // Suppress uglification warnings
                pure_getters: true,
                unsafe: true,
                unsafe_comps: true,
                screw_ie8: true,
            },
            output: {
                comments: false,
            },
            exclude: [/\.min\.js$/gi],  // skip pre-minified libs
        }),
    ],
};


module.exports.getProductionConfig = function (devConfig, prodExt) {
    return _.merge(devConfig, prodExt, (a, b) => {
        return _.isArray(a) ? a.concat(b) : _.merge(a, b);
    });
};
