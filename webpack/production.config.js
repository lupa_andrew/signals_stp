const devConfig = require('./config.js');
const { getProductionConfig, prodExtension } = require('./helpers');

const prodConfig = getProductionConfig(devConfig, prodExtension);

module.exports = prodConfig;
