const webpack = require('webpack');
const path = require('path');
const { version } = require('../package.json');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { getEntries } = require('./helpers');

const isProd = (process.env.NODE_ENV === 'production');

const paramToEntryMapper = {
    main: {
        'builds/style': './frontend/styles/style.less',
    },
};

function getPlugins() {
    const plugins = [];

    plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
        new ExtractTextPlugin({
            filename: `[name]${version}.css`,
            allChunks: true,
        })
    );

    // Conditionally add plugins for Production builds.
    if(isProd) {
        plugins.push(
            new OptimizeCssAssetsPlugin({
                cssProcessorOptions: {
                    autoprefixer: false,
                    zindex: false,
                    reduceIdents: false,
                    reducePositions: false,
                    reduceTransforms: false,
                    normalizeUrl: false,
                    mergeIdents: false,
                    discardUnused: false,
                    calc: false,
                },
            })
        );
    }
    return plugins;
}


module.exports = {
    entry: getEntries(paramToEntryMapper),
    output: {
        filename: `[name]-build-${version}.js`,
        path: path.resolve(__dirname, '../public/css'),
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader?url=false', 'less-loader'],
                }),
            },
        ],
    },
    plugins: getPlugins(),
    resolve: {
        modules: [
            path.resolve(__dirname, '../frontend/styles'),
            path.resolve(__dirname, '../frontend/node_modules'),
            'node_modules',
        ],
        extensions: ['.js', '.less'],
    },
};
